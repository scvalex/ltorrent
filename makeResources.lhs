#!/usr/bin/env runhaskell

> module Main where

> import Control.Monad
> import Data.Char
> import Data.List
> import System.Environment ( getArgs )

> makeHsString :: FilePath -> String -> String
> makeHsString f s = concat [ "(\"", f, "\", "
>                           , (++"':\"\"") . ('\'':) 
>                             . intercalate "':'" 
>                             . map (\s -> if s == "\n" then "\\n" else s)
>                             . map (\s -> if s == "\\" then "\\\\" else s)
>                             . map (:[]) $ s
>                           , ")"
>                           ]

> main :: IO ()
> main = do
>   putStrLn "module Resources where"
>   putStrLn ""
>   putStrLn "resources :: [(String, String)]"
>   putStrLn "resources ="
>   putStrLn "  ["
>   fs <- getArgs
>   forM_ fs $ \f -> do
>          putStr "  "
>          when (f /= head fs) $ putStr ", "
>          txt <- readFile f
>          putStrLn $ makeHsString f txt
>   putStrLn "  ]"
>   putStrLn ""
