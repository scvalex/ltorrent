PREFIX=/home/scvalex/proj/ltorrent/
ARGS=torrents/*.torrent
D=docker run --network host -v $(shell pwd)/build-root:/root -v $(shell pwd):$(shell pwd) -w $(shell pwd) -i -t haskell:latest

all: resources ltorrent

.PHONY: deps
deps:
	#$(D) cabal new-update
	$(D) cabal new-install Crypto HTTP HUnit QuickCheck json network network-uri old-locale random

.PHONY: ltorrent
ltorrent:
	$(D) cabal new-configure -f webtorrent -f dltorrent
	$(D) cabal new-build webtorrent dltorrent

.PHONY: resources
resources:
	$(D) runghc makeResources.lhs *.resource | grep -v "Loaded " > Resources.hs

.PHONY: clean
clean:
	$(D) cabal clean
