{-# LANGUAGE GeneralizedNewtypeDeriving, FlexibleContexts #-}

-- |
-- Module      :  LTorrent
-- Copyright   :  (c) Alexandru Scvortov 2009
-- License     :  LGPL (see LICENSE file)
-- Maintainer  :  scvalex@gmail.com
--

-- FIXME: -threaded runtime would sometimes lock the programme;
-- removed it.  I'm guessing it doesn't use a pool of threads for IO.

-- FIXME: lTorrent doesn't stop when file is completely downloaded.
-- On the other hand, no torrent has ever been _completely_
-- downloaded.

module LTorrent 
    ( startLTorrent
    , BridgeMessage
    ) where

import BitTorrent
import Control.Applicative
import Control.Concurrent
import Control.Concurrent.Extra
import Control.Concurrent.STM ( STM, TVar, TChan, atomically, check,
                                newTChanIO, readTChan, writeTChan,
                                newTVarIO, readTVar, writeTVar )
import Control.Exception.Extra
import Control.ITC hiding ( sendMessage )
import Control.Manager
import Control.Monad ( forM_, forever, when )
import Control.Monad.Reader
import Control.Monad.State
import Data.Array.Unboxed ( IArray, Ix, listArray
                          , assocs, accum, bounds
                          , (!), Array, (//), array
                          , elems )
import Data.Bencoding
import Data.ByteString.Extra
import qualified Data.ByteString.Lazy.Char8 as L
import Data.Char ( toLower )
import Data.Checksum
import Data.Extra
import Data.List ( foldl' )
import qualified Data.IntMap as M
import qualified Data.Set as S
import Data.Time.Clock
import Data.Time.Format
import Data.Maybe ( catMaybes )
import Network.Socket ( withSocketsDo
                      , connect
                      , addrSocketType
                      , defaultHints
                      , SocketType(..) 
                      , getAddrInfo
                      , socket
                      , addrFamily
                      , addrProtocol
                      , addrAddress
                      , socketToHandle
                      )
import Resources
import System.Environment ( getArgs )
import System.FilePath.Posix ( joinPath )
import System.IO
import Text.Printf ( printf )

debugMode = False

type FileTarget = (Integer, Integer, FilePath)

-- FIXME: TrackerInfo should have a lastUpdated attribute or
-- something.
-- FIXME: Maybe move TrackerInfo to BitTorrent.Tracker?
data TrackerInfo = TrackerInfo
    { getTrackerUrl :: String
    , getTrackerStatus :: String
    , getTrackerPeers :: Integer
    , getTrackerSeeds :: Integer
    }

instance BencodedData TrackerInfo where
    fromBencoding = undefined
    toBencoding ti = bencDictInsert ("url", getTrackerUrl ti)
                     $ bencDictInsert ("status", getTrackerStatus ti)
                     $ bencDictInsert ("peers", getTrackerPeers ti)
                     $ bencDictInsert ("seeds", getTrackerSeeds ti)
                     $ newBencoding

data TorrentConf = TorrentConf
                 { seqIO :: IO () -> IO () -- ^ do sequention IO
                 , doConnection :: ConnInstr -> IO () -- ^ use this to establish connections
                 , logTorrent :: String -> Torrent () -- ^ add something to the log
                 , logIO :: String -> IO () -- ^ add something to the log
                 , metainfo :: !Metainfo -- ^ metainfo for the current torrent
                 , downloadDirectory :: !FilePath -- ^ download directory
                 , myPeerId :: !PeerId -- ^ my peer id (something like
                                       -- -LT0000-123456654321)
                 , fileTargetMap :: M.IntMap [FileTarget] -- ^ where to write each piece
                 , peers :: TVar (S.Set Peer) -- ^ set of peers
                 , pieceStatus :: TVar PieceStatusArray -- ^ what's the status of a piece
                 , swarmPiecemap :: TVar Piecemap -- ^ the accumulated piecemap of the swarm
                 , getTrackerInfos :: [TVar TrackerInfo]
                 }

newtype Torrent a = Torrent (ReaderT TorrentConf IO a)
    deriving (Applicative, Functor, Monad, MonadIO, MonadReader TorrentConf)

-- | Run the Torrent monad, given a Torrent monad action and a
-- | configuration.  Return the result.
runTorrent :: TorrentConf -> Torrent a -> IO a
runTorrent c (Torrent a) = runReaderT a c

-- Do sequential IO.
ioSeq :: IO () -> Torrent ()
ioSeq a = asks seqIO >>= \si -> liftIO (si a)

-- Do concurrent IO.
io :: MonadIO m => IO a -> m a
io = liftIO

defaultChunkSize = 2^14

startLTorrent :: IO (TChan BridgeMessage)
startLTorrent = do
  inCh <- newTChanIO
  outCh <- newTChanIO
  noisySpawnIO (startTheBridge inCh) 
               (putStrLn "!!! LTorrent: theBridge has died !!!")
  return inCh

-- | This is LTorrent's link to the outside world.  It reads
-- instructions off the first channel and writes the result to the
-- second channel.
startTheBridge :: TChan BridgeMessage -> IO ()
startTheBridge inCh = do
  threadDelay 10000 -- otherwise the initial messages (that are not
                    -- sequenced) get jumbled up
  (lio, lto) <- setupLogging
  ioSeq <- setupIOSequencer
  doConn <- setupConnectionManager
  mihs <- setupTimeMaster
  fauxGo lio lto ioSeq doConn mihs
    where
      fauxGo lio lto ioSeq doConn mihs = go []
          where
            go :: [(InfoHash, TorrentConf)] -> IO ()
            go activeTorrents = do
              (i, o) <- atomically $ readTChan inCh
              case extractMessage i of
                Right Nop -> do
                        lio "lTorrent: got NOP" 
                        sendMessage o (BMR "NOP" "Ok, but why?" Nothing)
                        go activeTorrents
                Right (DownloadTorrent mi) -> do
                    lio "lTorrent: got DownloadTorrent; downloading:"
                    lio $ printf "lTorrent: \t %s" (getName mi)
                    mtc <- newEmptyMVar
                    forkIO $ startTorrentDownload mi lio lto ioSeq doConn mtc
                             `catchAll` (\e -> putMVar mtc Nothing)
                    tc <- takeMVar mtc
                    case tc of 
                      Nothing -> do
                                  sendMessage o (BMR "DownloadTorrent" "" (Just "startTorrentDownload erred"))
                                  go activeTorrents
                      Just torconf -> do
                                  --t <- getCurrentTime
                                  --writeConfigIfMissing [fromByteString $ getInfoHash mi, "startedOn"] (formatTime defaultTimeLocale "%s" t)
                                  atomically $ modifyTVar mihs (\ihs -> getInfoHash mi : ihs)
                                  sendMessage o (BMR "DownloadTorrent" "" Nothing)
                                  go ((getInfoHash mi, torconf) : activeTorrents)
                Right (GetPieceArray ih) -> do
                    lio "lTorrent: got GetPieceArray"
                    case map snd . filter (\(ih', _) -> ih == ih') $ activeTorrents of
                      [] -> do
                        lio "lTorrent: got GetPieceArray for inexistant InfoHash"
                        sendMessage o (BMR "GetPieceArray" "" (Just "no torrent with that infohash"))
                      (tc:_) -> do
                        psa' <- atomically $ getSendablePSA tc
                        spm' <- atomically $ getSendableSPM tc
                        let psas = bencDictInsert ("pieceStatus", psa')
                                 $ bencDictInsert ("swarmPieceMap", spm')
                                 $ newBencoding
                        sendMessage o (BMR "GetPieceArray" psas Nothing)
                        -- lio "lTorrent: sent back pieceStatusArray"
                    go activeTorrents
                Right GetTorrentList -> do
                    lio "lTorrent: got GetTorrentList"
                    let mis = map metainfo . map snd $ activeTorrents
                    let ps = map (\x -> mkBencDictFromList [ ("name", toBencoding $ getName x)
                                                           , ("infohash", toBencoding $ getInfoHash x) ]
                                 ) mis
                    let beps = map toBencoding ps

                    sendMessage o (BMR "GetTorrentList" beps Nothing)
                    go activeTorrents
                Right (GetTorrentInfo ih) -> do
                    lio "lTorrent: got GetTorrentInfo"
                    case map snd . filter (\(ih', _) -> ih == ih') $ activeTorrents of
                      [] -> do
                        lio "lTorrent: got GetTorrentInfo for inexistent InfoHash"
                        sendMessage o (BMR "GetTorrentInfo" "" (Just "no torrent with that infohash"))
                      (tc:_) -> do
                        psa <- atomically $ getSendablePSA tc
                        ps <- atomically $ getSendablePeers tc
                        te <- readConfigDefault [fromByteString ih, "timeElapsed"] "0"
                        ss <- atomically $ do
                          tis <- sequence . map readTVar . getTrackerInfos $ tc
                          let ps = map (\(TrackerInfo _ _ s l) -> (s, l)) tis
                          return $ foldl' (\(ts, tl) (s, l) -> (ts+s, tl+l)) (0, 0) ps
                        let resp = bencDictInsert ("metainfo", metainfo tc)
                                 $ bencDictInsert ("downloadDirectory", downloadDirectory tc)
                                 $ bencDictInsert ("peers", map toBencoding ps)
                                 $ bencDictInsert ("pieceStatus", psa)
                                 $ bencDictInsert ("timeElapsed", te)
                                 $ bencDictInsert ("leechers", fst ss)
                                 $ bencDictInsert ("seeders", snd ss)
                                 $ newBencoding
                        sendMessage o (BMR "GetTorrentInfo" resp Nothing)
                    go activeTorrents
                Right (GetTrackers ih) -> do
                    lio "lTorrent: got GetTrackers"
                    case map snd . filter (\(ih', _) -> ih == ih') $ activeTorrents of
                      [] -> do
                        lio "lTorrent: got GetTrackers for inexistent InfoHash"
                        sendMessage o (BMR "GetTrackers" "" (Just "no torrent with that infohash"))
                      (tc:_) -> do
                        bs <- atomically . sequence . map readTVar . getTrackerInfos $ tc
                        sendMessage o (BMR "getTrackers" (map toBencoding bs) Nothing)
                    go activeTorrents
                Right (GetPeers ih) -> do
                    case map snd . filter (\(ih', _) -> ih == ih') $ activeTorrents of
                      [] -> do
                        lio "lTorrent: got GetPeers for inexistent InfoHash"
                        sendMessage o (BMR "GetPeers" "" (Just "no torrent with that infohash"))
                      (tc:_) -> do
                        ps <- atomically $ readTVar (peers tc)
                        let ps' = S.elems . S.filter getIsEngaged $ ps
                        pcs <- mapM countHasPieces ps'
                        sps <- forM (zip ps' pcs) $ \(p, c) -> do
                            gc <- atomically . readTVar . getClient $ p
                            if gc == "Not responding"
                                then return Nothing
                                else do
                                  return $ Just $ bencDictInsert ("ip", getIP p)
                                                $ bencDictInsert ("numPieces", c)
                                                $ bencDictInsert ("client", gc)
                                                $ newBencoding
                        sendMessage o (BMR "getPeers" (catMaybes sps) Nothing)
                    go activeTorrents
                Left err -> do
                        lio $ printf "lTorrent: failed to parse message: %s" (err :: String)
                        sendMessage o (BMR "Unknown" "" (Just "failed to parse message"))
                        go activeTorrents
      countHasPieces :: Peer -> IO Integer
      countHasPieces p = do
            mpm <- atomically . readTVar $ getPiecemap p
            if mpm == Nothing
               then return 0
               else do
                 let (Just pm) = mpm
                 return . toInteger . length . filter id . elems $ pm
      getSendablePSA tc = do
            psa <- readTVar . pieceStatus $ tc
            return $ flip map (elems psa) $ \s -> case s of
                         Complete  -> 'c'
                         DontHave  -> 'd'
                         Getting _ -> 'g'
      getSendableSPM tc = do
            psa <- readTVar . swarmPiecemap $ tc
            return $ flip map (elems psa) $ \s -> if s then 'c' else 'd'
      getSendablePeers tc = do
           ps <- readTVar (peers tc)
           return $ S.toList ps


data (BencodedData a) => BridgeMessageResponse a = BMR 
    { getMessage :: String
    , getResponse :: a
    , getError :: Maybe String
    }

sendMessage :: (BencodedData a) => MVar Bencoding -> BridgeMessageResponse a -> IO ()
sendMessage o bmr = do
  let err = case getError bmr of
                Just err -> "Fail: " ++ err
                Nothing -> ""
  putMVar o $ bencDictInsert ("message", getMessage bmr)
            $ bencDictInsert ("response", getResponse bmr)
            $ bencDictInsert ("error", err)
            $ newBencoding

-- set up the two loging functions: one to be used from the IO monad
-- and the other from the Torrent monad
setupLogging :: IO (String -> IO (), String -> Torrent ())
setupLogging = do
  ltd <- getLTorrentDir
  createFilePathIfMissing ltd
  let logFile = ltd </> "log"
  logFileH <- openFile logFile WriteMode
  hSetBuffering logFileH LineBuffering
  let lio s = do
        t <- getCurrentTime
        hPutStrLn logFileH $ printf "%s: %s" (formatTime defaultTimeLocale "%T" t) s
        hFlush logFileH
        when debugMode $ printf "%s: %s\n" (formatTime defaultTimeLocale "%T" t) s
  let lto = io . lio
  return (lio, lto)

-- FIXME: Sometimes, the time elapsed jums randomly.  I'm willing to
-- bet that it's because of non-sequential IO.

-- Every 10 seconds, increment the timeElapsed of every torrent by
-- 10s.  This is not particularly accurate but it is a good enough
-- approximation.
-- Errors are handled by forked thread.
setupTimeMaster :: IO (TVar [InfoHash])
setupTimeMaster = do
  mihs <- newTVarIO []
  let loop = forever $ do
        ihs <- atomically $ readTVar mihs
        forM ihs $ \ih -> do
                    so <- readConfigDefault [fromByteString ih, "timeElapsed"] "0"
                    writeConfig [fromByteString ih, "timeElapsed"] 
                                (show (10 + (read so :: Integer)))
                                `catchAll` (\e -> putStrLn $ "Time Master: " ++  show e)
        threadDelay 10000000
  forkIO $ loop `finally` (putStrLn "!!! Time Master has died !!!")
  return mihs

-- some IO actions need to be run sequentially; setupIOSequencer forks
-- a thread that reads actions from a channel and executes them
-- sequentially
setupIOSequencer :: IO (IO () -> IO ())
setupIOSequencer = do
  ioCtrl <- newTChanIO
  forkIO $ (ioManager ioCtrl) 
           `finally` (putStrLn "!!! IO Manager has died !!!")
  return (atomically . writeTChan ioCtrl)


-- set up the connection manager -- a thread that manages network
-- connections; ALL connections should be established via the function
-- returned here
setupConnectionManager :: IO (ConnInstr -> IO ())
setupConnectionManager = do
  connCtrl <- newTChanIO
  forkIO $ (connectionManager connCtrl 600) 
           `finally` (putStrLn "!!! Connection Manager has died !!!")
  return (atomically . writeTChan connCtrl)

-- | Start a torrent download in the current thread.
startTorrentDownload :: Metainfo -> (String -> IO ()) -> (String -> Torrent ())
                     -> (IO () -> IO ()) -> (ConnInstr -> IO ())
                     -> MVar (Maybe TorrentConf) -> IO ()
startTorrentDownload mi lio lto ioSeq doConn mtc = do
  case lookup "banner.resource" resources of
    Just banner -> lio $ banner

  peerSet <- newTVarIO S.empty

  dddir <- getDefaultDownloadDir
  let ddir = dddir </> (getName mi)
  wasNewDir <- createFilePathIfMissing ddir
  lio $ printf "LTorrent: downloading %s to\n\t%s\n\n" (getName mi) ddir

  let ftmap = buildFileTargetMap mi

  mpid <- makeMyPeerId

  let len = (length . getPieceList $ mi) + 7 -- the "+ 7" here is to avoid 
                                              -- array addition errors
  spm <- newTVarIO $ listArray (0, len) (repeat False)
  ps <- newTVarIO $ listArray (0, len) (repeat DontHave)

  trs <- sequence . map (newTVarIO . initTracker) . getTrackers $ mi

  let iconf = TorrentConf ioSeq doConn lto lio mi ddir mpid ftmap peerSet ps spm trs

  putMVar mtc (Just iconf)

  -- FIXME: generateInitialPieceStatus should have some sort of error
  -- correction; worst case would be to delete everything downloaded
  -- so far
  when (not wasNewDir) $ generateInitialPieceStatus iconf

  lio $ printf "LTorrent: Torrent has %d pieces of %d bytes" 
               (length . getPieceList $ mi)
               (getPieceLength mi)
  let tsize = getPieceLength mi * (fromIntegral . length . getPieceList $ mi)
  -- FIXME: use something like dltorrent.showPrettyFilesize instead of
  -- the hardwired calculations here
  lio $ printf "LTorrent: Total size: %d bytes\n\t%d kB\n\t%f MB\n" 
         tsize (tsize `div` 1024) (fromIntegral (tsize `div` 1024) / 1000 :: Float)

  forkIO $ (trackerManager iconf)
           `finally` (lio "lTorrent: trackerManager has died")
  
  forkIO $ (peerManager iconf 100 peerHandler) 
           `finally` (lio "lTorrent: peerManager has died") 

  return ()
      where
        initTracker t = TrackerInfo t "Not Contacted" 0 0


generateInitialPieceStatus :: TorrentConf -> IO ()
generateInitialPieceStatus conf = do
  let mi = metainfo conf
      ps = pieceStatus conf
      lio = logIO conf
  lio ">>> FOUND pieces: "
  forM_ [0..(length . getPieceList $ mi)-1] $ \i -> do  -- TODO NOT optimal
     txt <- runTorrent conf (readPieceFromDisk i)
     let cksum = toByteString . sha1 . fromByteString $ txt -- TODO this is depressingly inefficient
     let expCksum = bytesToHexGroups $ getPieceList mi !! i
     when (cksum == expCksum) $ do
       lio $ printf "\t%d " i
       atomically $ modifyTVar ps (\a -> a // [(i, Complete)])
  lio "\n"

-- FXIME Maybe use HTTP.Browser instead of vanilla connections in
-- trackerManager?

-- | trackerManager takes care of querying the trackers for new peers.
-- Every 60 minutes, it hits every tracker and pools the results
-- togather in the peerset.
trackerManager :: TorrentConf -> IO ()
trackerManager conf = do
  let ir = (makeInitialRequest . metainfo $ conf) { getPeerId = myPeerId conf }
  let tmLoop = do
        forM_ (getTrackerInfos conf) $ \ti -> do
                        (TrackerInfo t _ _ _) <- atomically . readTVar $ ti
                        doConnection conf $ ConnInstr (doTrackerQuery t ir (processTrackerResponse conf ti))
                                                      (Just 10000)
                                                      (doTrackerFailed conf ti)
                                                      (return ())
        inDo (30*60*1000) $ tmLoop -- every 30 mins
  tmLoop

updateStats :: TVar TrackerInfo -> (Integer, Integer) -> STM ()
updateStats ti (nl, ns) = modifyTVar ti $ \(TrackerInfo t _ nls nss) -> TrackerInfo t "Ok" (nls+nl) (nss+ns)

doTrackerFailed :: TorrentConf -> TVar TrackerInfo -> String -> IO ()
doTrackerFailed tc ti s = do
  let em = "trackerQuery failed: " ++ s
  logIO tc em
  atomically $ modifyTVar ti $ (\t -> t { getTrackerStatus = "Failed" })

processTrackerResponse :: TorrentConf -> TVar TrackerInfo -> TrackerResponse -> IO ()
processTrackerResponse conf ti resp = do
  let ps = peers conf
  mergeToPeerSet ps resp
  atomically $ updateStats ti (getCompleteIncomplete resp)

mergeToPeerSet :: TVar (S.Set Peer) -> TrackerResponse -> IO ()
mergeToPeerSet peerSet tr = do
  peers <- forM (getPeers tr) $ \(i, p) -> newPeerIO i p
  atomically $ modifyTVar peerSet (\ps -> S.union ps (S.fromList peers))
--  xs <- atomically $ readTVar peerSet
--  putStrLn $ "peers so far: " ++ (show . S.size $ xs)
  return ()

-- | peerManager takes care of communication with peers; it talks to
-- them, gathers statistics and decides which are interesting.
--
-- NOTE: peerManager should be run as a separate thread
peerManager :: TorrentConf 
            -> Int -- ^ maximum number of contacted peers per torrent
            -> (TorrentConf -> Peer -> IO ()) -- ^ peer handler
            -> IO ()
peerManager conf mpeers ph = do
  connAlive <- newTVarIO 0
  pmLoop connAlive
    where
      pmLoop :: TVar Int -> IO ()
      pmLoop connAlive = do
        let ps = peers conf
        sa <- atomically $ do
                 stillAlive <- readTVar connAlive
                 check (stillAlive < mpeers)
                 return stillAlive
        let lio = logIO conf
        lio $ printf "still alive: %d" (sa :: Int)
        psa' <- atomically . readTVar . pieceStatus $ conf
        when ((length . filter (/= Complete) . elems $ psa') > 0) $ do
            peerSet <- atomically . readTVar $ ps
            let vah = (filter (not . getIsEngaged) . S.elems $ peerSet)
            lio $ printf "not alive: %d" (length vah)
            r <- takeRandom vah
            case r of
              Just cp -> do
                     atomically $ do
                           modifyTVar connAlive (+1)
                           modifyTVar ps (\pl -> replace pl cp (cp {getIsEngaged = True}))
                     let conn = doConnection conf
                     conn $ ConnInstr (ph conf cp)
                                      Nothing
                                      (lio . ("peerHandler failed: "++))
                                      (atomically $ do {
                                          modifyTVar connAlive (\i -> i - 1);
                                          modifyTVar ps (\pl -> replace pl cp (cp {getIsEngaged = False }))
                                      })
              Nothing -> return () -- putStrLn "no unchecked peer"
            inDo 1000 $ pmLoop connAlive

replace :: Ord a => S.Set a -> a -> a -> S.Set a
replace s x y = S.insert y $ S.delete x s

standardPeerTimeout :: IO a -> IO (Maybe a)
standardPeerTimeout = timeoutMaybe 120000

extractMaybe :: MonadIO m => String -> Maybe a -> m a
extractMaybe reason Nothing = fail reason
extractMaybe _ (Just x) = return x

extendPeer :: TorrentConf -> Peer -> IO Peer
extendPeer conf p = do
  spm <- atomically . readTVar . swarmPiecemap $ conf
  let pm = listArray (bounds spm) (repeat False)
  atomically $ writeTVar (getPiecemap p) (Just pm)
  return p

-- FIXME: peerHandler and a few of the others smell like a state
-- machine pattern.  Try to abstract it.

peerHandler :: TorrentConf -> Peer -> IO ()
peerHandler conf ap = do
  let lio = logIO conf
  lio $ "pH: contacting " ++ (getIP ap)
  let ih = getInfoHash . metainfo $ conf
  let mpid = myPeerId conf
  withSocketsDo $ do
    (h, p) <- establishConnection ap 
    do { -- SETUP CONNECTION
         initLoop lio h p ih mpid
       ; -- MAIN CONNECTION LOOP
         runTorrent conf $ msgLoop h p
    } `finally` (hClose h)
      where
        establishConnection :: Peer -> IO (Handle, Peer)
        establishConnection ap = do
            let hints = defaultHints { addrSocketType = Stream }
            addr:_ <- getAddrInfo (Just hints) (Just (getIP ap)) (Just (show (getPort ap)))
            sock <- socket (addrFamily addr) (addrSocketType addr) (addrProtocol addr)
            extractMaybe (printf "pH: initial connection timed out (peer %s)" (getIP ap))
              =<< (timeoutMaybe 9000 $ connect sock (addrAddress addr))
            h <- socketToHandle sock ReadWriteMode
            extendPeer conf ap
            return (h, ap)

        initLoop :: (String -> IO ()) -> Handle -> Peer -> L.ByteString -> PeerId -> IO ()
        initLoop lio h p ih mpid = do
            -- HANDSHAKE - send and get
            hPutHandshake h $ Handshake ih mpid
            hs <- extractMaybe (printf "pH: peer %s timed out during handshake" (getIP p)) 
               =<< (timeoutMaybe 15000 $ hGetHandshake h)
            lio $ printf "pH: Got handshake from %s; other peer is %s." 
                         (getIP p) (translatePeerId . getHandshakePeerId $ hs)
            atomically . writeTVar (getClient p) . translatePeerId . getHandshakePeerId $ hs

        msgLoop :: Handle -> Peer -> Torrent ()
        msgLoop h p = do
            msg <- extractMaybe (printf "pH: peer %s timed out during message loop" (getIP p)) 
                =<< (io $ standardPeerTimeout $ (hGetMessage h))
            lto <- asks logTorrent
            let ipa = getIP p
            case msg of
              BitField bs -> do
--                       io $ putStrLn "pH: got a bitfield"
                       let pm' = makePiecemap bs
                       spm <- asks swarmPiecemap
                       io $ atomically $ modifyTVar spm (\opm -> zipArrayWith (||) opm pm')
                       mi <- asks metainfo
                       lto $ printf "pH: peer %s has %d / %d pieces" ipa (length . filter id . elems $ pm') (length $ getPieceList mi)
                       io $ hPutMessage h Interested >> hPutMessage h Unchoke
                       io $ setPiecemap p pm'
                       msgLoop h p
              Choke -> do
                       lto $ printf "pH: peer %s choked me!" ipa
                       io $ setPeerChocked p True
                       msgLoop h p
              Unchoke -> do
                         lto $ printf "pH: peer %s UNchoked me!" ipa
                         requestRandomPiece h p
                         io $ setPeerChocked p False
                         msgLoop h p
              Interested -> do
                         lto $ printf "pH: peer %s is interested!" ipa
                         io $ setPeerInterested p True
                         msgLoop h p
              NotInterested -> do
                         lto $ printf "pH: peer %s is NOT interested!" ipa
                         io $ setPeerInterested p False
                         msgLoop h p
              KeepAlive -> do
                         lto $ printf "pH: peer %s sent me a keep-alive!" ipa
                         msgLoop h p
              Have i -> do
                         --io $ printf "pH: peer %s has piece %d!" ipa i
                         spm <- asks swarmPiecemap
                         io $ atomically $ modifyTVar spm (// [(i, True)])
                         mpm <- io $ readPiecemap p
                         io $ when (mpm /= Nothing) $ let (Just pm) = mpm in setPiecemap p (pm // [(i, True)]) >> return ()
                         msgLoop h p
              Request i beg len -> do
                         lto $ printf "pH: peer %s has requested piece %d! (pity really)" ipa i
                         msgLoop h p
              RequestedPiece i beg blk -> do
                         let beg' = beg + defaultChunkSize
                         writePieceToDisk i (fromIntegral beg) blk
                         pl <- getPieceLength <$> asks metainfo
                         lto $ printf "pH: peer %s progress: %d/%d" ipa beg pl
                         if (fromIntegral beg') < pl
                            then do
                              io $ hPutMessage h (Request i beg' defaultChunkSize)
                            else do
                              checkPiece i =<< asks pieceStatus 
                              requestRandomPiece h p
                         msgLoop h p
              Cancel i beg len -> do
                         lto $ printf "pH: peer %s has canceled piece %d! :(" ipa i
                         msgLoop h p
              _ -> msgLoop h p

requestRandomPiece :: Handle -> Peer -> Torrent ()
requestRandomPiece h p = do
  mpm <- io $ readPiecemap p
  when (mpm /= Nothing) $ do
    let (Just pm) = mpm
    ps <- asks pieceStatus
    lio <- asks logIO
    io $ do
      i' <- getRandomAvailablePiece pm ps
      mtid <- myThreadId
      atomically $ modifyTVar ps (\a -> modifyArray a i' (\v -> case v of
                                                                  Getting xs -> Getting (mtid:xs)
                                                                  _          -> Getting [mtid]))
      lio $ printf ">>> grabbing piece %d from %s" i' (getIP p)
      hPutMessage h (Request i' 0 defaultChunkSize)

modifyArray :: (IArray a e, Ix i) => a i e -> i -> (e -> e) -> a i e
modifyArray a i f = a // [(i, f (a ! i))]

checkPiece :: Int -> TVar PieceStatusArray -> Torrent ()
checkPiece i ps = do
  mi <- asks metainfo
  txt <- readPieceFromDisk i
  lio <- asks logIO
  io $ do
    lio $ printf ">>> CHECKING piece %d (%d bytes)" i (L.length txt)
    let cksum = L.pack $ sha1 (L.unpack txt) -- TODO this is depressingly inefficient
        expCksum = bytesToHexGroups $ getPieceList mi !! i
--    lto $ printf ">>> CHECKSUM %s" (L.unpack cksum)
--    lto $ printf ">>> EXPECTED %s" (L.unpack expCksum)
    if (cksum == expCksum)
         then do
           lio $ printf ">>> CHECKSUM OK ;)\n"
           atomically $ modifyTVar ps (\a -> a // [(i, Complete)])
         else do
           lio $ printf ">>> CHECKSUM FAILED :(\n"
           atomically $ modifyTVar ps (\a -> a // [(i, DontHave)])
   
getRandomAvailablePiece :: Piecemap -> TVar PieceStatusArray -> IO Int
getRandomAvailablePiece pm tps = do
  let len = snd . bounds $ pm
  j <- randint (0, len-1) -- see last line of function
  ps <- atomically $ readTVar tps
  let loop i = do
        if (i >= len) 
          then loop 0
          else if i == j
            then fail "no pieces available"
            else if (pm ! i && ps ! i /= Complete)
              -- I'm going to hell for this.  A piece can now be
              -- retrieved from more than one source.
              then return i
              else loop (i+1)
  loop (j+1)
  
zipArrayWith :: (IArray a e, Ix i) => (e -> e -> e) -> a i e -> a i e -> a i e
zipArrayWith op arr brr = accum op arr (assocs brr)

-- FIXME: sometimes complains that file is locked for writing; as
-- writes are (read should be) done through the IO Manager, this
-- shouldn't be possible

writePieceToDisk :: Int -> Integer -> L.ByteString -> Torrent ()
writePieceToDisk i beg blk = do
  ftm <- asks fileTargetMap
  case M.lookup i ftm of
    Just fs -> go beg blk fs
    Nothing -> fail "writePieceToDisk: couldn't lookup piece in dict"
    where
       go :: Integer -> L.ByteString -> [FileTarget] -> Torrent ()
       go _ _ [] = return ()
       go offset blk ((b, e, fp):fs) = do
          let filePartSize = e - b
          if offset > filePartSize
             then go (offset - filePartSize) blk fs
             else do
               let filePartSize' = fromIntegral (filePartSize - offset)
               trgtFile <- (</> fp) <$> asks downloadDirectory
               let dataSize = fromIntegral (L.length blk)
               if dataSize <= filePartSize'
                  then do
                    ioSeq $ writeFilePart trgtFile (fromIntegral (b+offset)) blk
                  else do
                    ioSeq $ writeFilePart trgtFile (fromIntegral (b+offset)) (L.take filePartSize' blk)
                    go 0 (L.drop filePartSize' blk) fs

readPieceFromDisk :: Int -> Torrent L.ByteString
readPieceFromDisk i = do
  ftm <- asks fileTargetMap
  case M.lookup i ftm of
      Just fs -> go L.empty fs
      Nothing -> fail "readPieceToDisk: couldn't lookup piece in dict"
   where
      go acc [] = return acc
      go acc ((b, e, fp):fs) = do
        trgtFile <- (</> fp) <$> asks downloadDirectory
        txt <- io $ readFilePart trgtFile b (e-b)
        go (acc `L.append` txt) fs

-- write data to file offseted
writeFilePart :: FilePath -> Integer -> L.ByteString -> IO ()
writeFilePart fp offset blk = do
  withBinaryFile fp ReadWriteMode $ \h -> do
    hSeek h AbsoluteSeek offset
    L.hPut h blk

-- read n bytes of data from file starting with offset
readFilePart :: FilePath -> Integer -> Integer -> IO L.ByteString
readFilePart fp offset n =
  withBinaryFile fp ReadWriteMode $ \h -> do
    hSeek h AbsoluteSeek offset
    bs <- L.hGet h (fromIntegral n)
    return $! bs

-- Given a torrent and a piece index, find the list of physical
-- files the piece needs to be written to.
--
-- The difficulty lies in that files rarely match piece boundries.
-- For instance, in a torrent with 2MB pieces made out of small text
-- files, a single piece may contain more than one file and a file may
-- be split between pieces.
--
-- TODO figure out the complexity for this
buildFileTargetMap :: Metainfo -> M.IntMap [FileTarget]
buildFileTargetMap mi =
    let fs = map (\(sz, _, pths) -> (0, sz, joinPath pths)) . getFileList $ mi
        plen = getPieceLength $ mi
        num = length . getPieceList $ mi
    in evalState (buildEntries plen num) fs

-- FIXME: The Writer monad seems like a better choice than State for
-- LTorrent.buildEntries.

buildEntries :: Integer -> Int -> State [FileTarget] (M.IntMap [FileTarget])
buildEntries plen num = go 0 num M.empty
    where
      go :: Int -> Int -> M.IntMap [FileTarget] -> State [FileTarget] (M.IntMap [FileTarget])
      go _ 0 acc   = return acc
      go i num acc = do
        p <- fillPiece plen []
        go (i+1) (num-1) (M.insert i p acc)

fillPiece :: Integer -> [FileTarget] -> State [FileTarget] [FileTarget]
fillPiece plen acc = do
  fs <- get
  if (length fs == 0) || (plen == 0)
     then return $ reverse acc
     else do
       let (b, e, fp) = head fs
       if e - b <= plen
         then do
           put (tail fs)
           fillPiece (plen - (e-b)) ((fromIntegral b, fromIntegral e, fp):acc)
         else do
           put ((b + plen, e, fp):tail fs)
           fillPiece 0 ((fromIntegral b, fromIntegral (b+plen), fp):acc)
