-- |
-- Module      :  Text.JSON.Bencoding
-- Copyright   :  (c) Alexandru Scvortov 2009
-- License     :  LGPL (see LICENSE file)
-- Maintainer  :  scvalex@gmail.com
--
-- JSON instance for Bencoding
--

module Text.JSON.Bencoding
    ( Bencoding
    ) where

import Data.Bencoding
import qualified Data.Map as M
import Text.JSON

instance JSON Bencoding where
    showJSON (BencString bs) = showJSON bs
    showJSON (BencInt i) = showJSON i
    showJSON (BencList xs) = showJSON $ map showJSON xs
    showJSON (BencDict m) = makeObj $ M.toList $ M.map showJSON m
    readJSON = undefined
