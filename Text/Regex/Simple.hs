-- |
-- Module      :  Text.Regex.Simple
-- Copyright   :  (c) Alexandru Scvortov 2008
-- License     :  LGPL (see LICENSE file)
-- Maintainer  :  scvalex@gmail.com
--

module Text.Regex.Simple
    ( matchPattern
    ) where

-- | Attempt to match a pattern over a text.  All characters except
-- | for '.' are treated literally: 'a' will match any text beginning
-- | with 'a', 'bc' will match any text beginning with 'bc'.  The dot
-- | ('.') is a wildcard that matches ANY ONE character.  So, '.' will
-- | match any non-null string, and 'a.a' will match strings like
-- | 'aaa', 'aBa' and 'a5acd'.
matchPattern :: String -- ^ the pattern
             -> String -- ^ the text
             -> Bool
matchPattern [] _ = True
matchPattern pat [] = False
matchPattern ('.':xs) (y:ys) = matchPattern xs ys
matchPattern (x:xs) (y:ys)
                 | x == y    = matchPattern xs ys
                 | otherwise = False
