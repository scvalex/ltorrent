{-# LANGUAGE TypeSynonymInstances, FlexibleInstances, UndecidableInstances #-}

-- |
-- Module      :  Data.Bencoding
-- Copyright   :  (c) Alexandru Scvortov 2009
-- License     :  LGPL (see LICENSE file)
-- Maintainer  :  scvalex@gmail.com
--

module Data.Bencoding
    ( Bencoding(..), BencodedData(..)
    , showStructure
    , decode, parseBencoding
    , encode, encodeString
    , bencDictLookup, mkBencDictFromList
    , IsByteString(..)
    , newBencoding, bencDictInsert
    ) where

import Text.Parsec
import Text.Parsec.ByteString.Lazy
import Text.Printf ( printf )
import qualified Data.Map as Map
import Data.ByteString.Extra
import qualified Data.ByteString.Lazy.Char8 as L
import Data.Semigroup

-- | From <http://en.wikipedia.org/wiki/Bencode>
-- Bencode (pronounced "Bee-Encode") is the encoding used by the
-- peer-to-peer file sharing system BitTorrent for storing and
-- transmitting loosely structured data.
--
-- It supports four different types of values:
--   * byte strings,
--   * integers,
--   * lists, and
--   * dictionaries (associative arrays).
data Bencoding = BencString L.ByteString
               | BencInt Integer
               | BencList [Bencoding]
               | BencDict (Map.Map String Bencoding)
                 deriving (Eq)

instance Show Bencoding where
    show = showStructure

instance Semigroup Bencoding where
  _ <> _ = newBencoding

-- | The Monoid instance of Bencoding is only defined for
-- dictionaries; attempts to use it on any of the other types will
-- result in mempty.
instance Monoid Bencoding where
    mempty = newBencoding
    (BencDict a) `mappend` (BencDict b) = BencDict $ Map.union b a
    _ `mappend` _ = mempty

-- | The 'BencodedData' class represents data that can be extracted from
-- a 'Bencoding' (ie strings, integers, lists and maps).
class BencodedData a where
    fromBencoding :: (Monad m) => Bencoding -> m a
    toBencoding :: a -> Bencoding

instance BencodedData Integer where
    fromBencoding (BencInt i) = return i
    fromBencoding _           = fail "can't convert to Integer"
    toBencoding = mkBencInt

instance BencodedData String where
    fromBencoding (BencString s) = return . L.unpack $ s
    fromBencoding _              = fail "can't convert to String"
    toBencoding = mkBencString

instance BencodedData L.ByteString where
    fromBencoding (BencString s) = return s
    fromBencoding _              = fail "can't convert to lazy ByteString"
    toBencoding = mkBencString

instance BencodedData Bencoding where
    fromBencoding = return . id
    toBencoding = id

-- FIXME: There should be a BencodedData [a] instance.
-- Note: We cannot change instance BencodedData [Bencoding] to
-- instance (BencodedData a) => [a] because weird things can happen
-- then (I do not know what).
-- Scratch that.  Generalizing this to BencodedData a => BencodedData
-- [a] ovelaps with BencodedData String.
instance BencodedData [Bencoding] where
    fromBencoding (BencList xs) = return xs
    fromBencoding _             = fail "can't convert to list"
    toBencoding = mkBencList

instance BencodedData (Map.Map String Bencoding) where
    fromBencoding (BencDict d) = return d
    fromBencoding _            = fail "can't convert to Map"
    toBencoding = mkBencDict

instance (BencodedData a, BencodedData b) => BencodedData (a, b) where
    fromBencoding = undefined
    toBencoding (a, b) = mkBencList [toBencoding a, toBencoding b]

-- FIXME: Data.Bencoding.showStructure is somewhat wasteful when
-- called on a BencList or a BencDict.
showStructure :: Bencoding -> String
showStructure (BencString s) = printf "%d byte string" (L.length s)
showStructure (BencInt i) = show i
showStructure bl@(BencList [x]) = if (length . lines . showStructure $ x) == 1
                               then "[" ++ showStructure x ++ "]"
                               else showBencList [x]
showStructure (BencList xs) = showBencList xs 
showStructure (BencDict m) = "{\n" ++ (unlines . map ("  "++) .  concatMap lines . map (\(k, v) -> k ++ " ==>" ++ showValue v) . Map.assocs $ m) ++ "}"
    where
      showValue v = if (length . lines . showStructure $ v) == 1
                    then ' ' : showStructure v
                    else '\n' : (unlines . map ("  "++) . lines . showStructure $ v)

showBencList xs = "[\n" ++ (unlines . map ("  "++) . concatMap (lines . showStructure) $ xs) ++ "]"

-- | An empty bencoded dictionary.
newBencoding :: Bencoding
newBencoding = BencDict Map.empty

-- | Insert a new (key, value) pair into a bencoded dcitionary.
bencDictInsert :: BencodedData a => (String, a) -> Bencoding -> Bencoding
bencDictInsert (key, val) (BencDict m) = BencDict $ Map.insert key (toBencoding val) m
bencDictInsert _ _ = mkBencString "failed dict insert; not a dict"

-- | Look up a value in a 'BencDict'.  Covenience function that unwraps
-- the dictionary and performes the lookup.
bencDictLookup :: (Monad m) => String -> Bencoding -> m Bencoding
bencDictLookup s (BencDict d) = case Map.lookup s d of
                                  Nothing -> fail $ printf "value %s not found in dictionary" s
                                  Just v  -> return v
bencDictLookup _ _ = fail "bencoded data is not a dictionary"

-- | Convert a String into a bencoded one.
mkBencString :: (IsByteString a) => a -> Bencoding
mkBencString = BencString . toByteString

mkBencInt :: Integer -> Bencoding
mkBencInt = BencInt

mkBencList :: (BencodedData a) => [a] -> Bencoding
mkBencList = BencList . map toBencoding

mkBencDict :: BencodedData a => Map.Map String a -> Bencoding
mkBencDict = BencDict . Map.mapWithKey (\k v -> toBencoding v)

-- FIXME: Rewrite mkBencDictFromList as an instance of BencodedData.

mkBencDictFromList :: (BencodedData a) => [(String, a)] -> Bencoding
mkBencDictFromList = mkBencDict . Map.fromList

-- | Encode a Bencoding as a lazy 'ByteString'.
--
-- NOTE: Encoded means already put into a string-like form.
encode :: IsByteString a => Bencoding -> a
encode = fromByteString . go
    where
      go (BencString s) = mconcat [toByteString . show . L.length $ s, toByteString ':', s]
      go (BencInt i) = mconcat [toByteString 'i', toByteString $ show i, toByteString 'e']
      go (BencList []) = toByteString "le"
      go (BencList xs) = mconcat [toByteString 'l', mconcat . map go $ xs, toByteString 'e']
      go (BencDict d)
          | Map.null d  = toByteString "de"
          | otherwise   =  mconcat [ toByteString 'd'
                                   , mconcat . map (\(k, v) -> mconcat [go (BencString . toByteString $ k), go v]) $ Map.toList d
                                   , toByteString 'e' ]

-- | Encode a 'Bencoding' as an 'IsByteString'.  Convenience function built around 'encode'.
encodeString :: IsByteString a => Bencoding -> a
encodeString = fromByteString . encode

-- | Build the 'Bencoding' from a lazy ByteString.
decode :: (Monad m, IsByteString a) => a -> m Bencoding
decode input = case parse benc "" $ toByteString input of
                 Left er -> fail $ show er
                 Right x -> return x

-- | Build a 'Bencoding' from an 'IsByteString'.  Convenience function built around 'decode'.
parseBencoding :: IsByteString a => a -> Maybe Bencoding
parseBencoding = decode . toByteString

benc = choice [try bencDict, try bencString, try bencInt, bencList]
       <?> "bencoding"

bencDict = do
  char 'd'
  xs <- many bencDictEntry
  char 'e'
  return $ BencDict (Map.fromList xs)
  <?> "bencoding dictionary"

bencDictEntry = do
  k <- bencString
  v <- benc
  return (bencsToS k, v)

-- this really should be BencString -> String
bencsToS :: Bencoding -> String
bencsToS (BencString s) = L.unpack s

bencString = do
  l <- number
  char ':'
  s <- count (fromInteger l) anyToken
  return $ BencString (L.pack s)
  <?> "bencoding string"

number = do
  n <- many1 (digit <|> char '-')
  return $ read n

bencInt = do
  char 'i'
  n <- number
  char 'e'
  return $ BencInt n
  <?> "bencoding integer"

bencList = do
  char 'l'
  xs <- many benc
  char 'e'
  return $ BencList xs
  <?> "bencoding list"
