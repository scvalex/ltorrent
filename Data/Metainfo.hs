{-# LANGUAGE BangPatterns, ExistentialQuantification #-}

-- |
-- Module      :  Data.Metainfo
-- Copyright   :  (c) Alexandru Scvortov 2009
-- License     :  LGPL (see LICENSE file)
-- Maintainer  :  scvalex@gmail.com
--
-- I'm using this as a spec
-- <http://wiki.theory.org/BitTorrentSpecification>
--

-- FIXME: Check whether Metainfos toBencoding works (i.e. add tests).

module Data.Metainfo 
    ( Metainfo(..), InfoHash
    , parseMetainfo
    ) where

import Data.Bencoding
import qualified Data.ByteString.Lazy.Char8 as L
import Data.ByteString.Extra
import Data.Checksum
import Data.List
import Data.Maybe

type InfoHash = L.ByteString

-- | Stores all the information in a metainfo (.torrent) file.
data Metainfo = Metainfo {
      -- | Combines both announce and announce-list fields as a list
      -- of Strings, each the URL of a tracker.
      getTrackers :: [String]

      -- | Optional. The creation time of the torrent, in standard
      -- UNIX epoch format (integer seconds since 1-Jan-1970
      -- 00:00:00 UTC).
    , getCreationDate :: Maybe Integer

      -- | Optional. Free-form textual comments of the author.
    , getComment :: Maybe String

      -- | Optional. Name and version of the program used to create
      -- the .torrent.
    , getCreatedBy :: Maybe String

      -- | Number of bytes in each piece. In most cases this will be a
      -- power of 2.
    , getPieceLength :: Integer

      -- | List of Strings containing the 20-byte SHA1
      -- | hash values, one per piece.
    , getPieceList :: [L.ByteString]

      -- | Optional. This field is an integer. If it is set to "1",
      -- the client MUST publish its presence to get other peers
      -- ONLY via the trackers explicitly described in the metainfo
      -- file. If this field is set to "0" or is not present, the
      -- client may obtain peer from other means, e.g. PEX peer
      -- exchange, DHT. Here, "private" may be read as "no external
      -- peer source".
    , getPrivate :: Maybe Integer

      -- | The filename of the directory in which to store all the
      -- files. This is purely advisory.
    , getName :: String

      -- | A list of touples, one for each file. Each touple in this
      -- list contains the following values:
      --    - length of the file in bytes;
      --    - Optional. A 32-character hexadecimal string corresponding
      --      to the MD5 sum of the file. This is not used by
      --      BitTorrent at all, but it is included by some programs
      --      for greater compatibility;
      --    - a list containing one or more string elements that
      --      together represent the path and filename. Each element
      --      in the list corresponds to either a directory name or
      --      (in the case of the final element) the filename. For
      --      example, a the file "dir1/dir2/file.ext" would consist
      --      of three string elements: "dir1", "dir2", and
      --      "file.ext". 
    , getFileList :: [(Integer, Maybe String, [String])]

      -- | Extra. The SHA1 hash of the info dictionary. This is used
      -- to uniquely identify a torrent to a tracker.
    , getInfoHash :: InfoHash
    } 
                deriving (Show, Eq)

data StringBencodedPair = forall a. BencodedData a => SBP String (Maybe a)

instance BencodedData Metainfo where
    fromBencoding be = case bencodingToMetainfo be of
                         Nothing -> fail "Couldn't extract metainfo from bencoding"
                         Just mi -> return mi
    toBencoding mi = be
        where
          hs :: [StringBencodedPair]
          hs = [ SBP "announce-list" (Just (map toBencoding . getTrackers $ mi))
               , SBP "comment" (getComment mi)
               , SBP "creation date" (getCreationDate mi)
               , SBP "created by" (getCreatedBy mi) ]
          is :: [StringBencodedPair]
          is = [ SBP "piece length" (Just . getPieceLength $ mi)
               , SBP "pieces" (Just . mconcat . getPieceList $ mi)
               , SBP "private" (getPrivate mi)
               , SBP "name" (Just . getName $ mi)
               , SBP "files" (Just . map makeFileTriplet . getFileList $ mi) ]
          infoDict = [("info", mkBencDictFromList 
                               . map (\(SBP k (Just v)) -> (k, toBencoding v)) 
                               . filter (\(SBP _ v) -> case v of 
                                                         Nothing -> False
                                                         _       -> True) 
                               $ is)]
          be = mkBencDictFromList $ (map (\(SBP k (Just v)) -> (k, toBencoding v))
                                    . filter (\(SBP _ v) -> case v of
                                                              Nothing -> False
                                                              _       -> True)
                                    $ hs)
                                  ++ infoDict
          makeFileTriplet :: (Integer, Maybe String, [String]) -> Bencoding
          makeFileTriplet (s, md5, ps) = bencDictInsert ("length", toBencoding s)
                                       $ bencDictInsert ("path", toBencoding . map toBencoding $ ps)
                                       $ newBencoding

emptyMetainfo :: Metainfo
emptyMetainfo = Metainfo 
                   []        -- getTrackers
                   Nothing   -- getCreationDate
                   Nothing   -- getComment
                   Nothing   -- getCreatedBy
                   0         -- getPieceLength
                   []        -- getPiecesList
                   Nothing   -- getPrivate
                   ""        -- getName
                   []        -- getFileList
                   L.empty   -- getInfoHash

-- | Parse a lazy 'ByteString' info a 'Metainfo'.
parseMetainfo :: IsByteString a => a -> Maybe Metainfo
parseMetainfo input = decode (toByteString input) >>= bencodingToMetainfo

-- This is the heart of Data.Metainfo.  As its name sugests,
-- bencodingToMetainfo takes a Bencoding and attempts get a Metainfo
-- from it.  This function also handles all unusual cases such as
-- optional values, missing fields and malformed fields.
bencodingToMetainfo :: Bencoding -> Maybe Metainfo
bencodingToMetainfo bd = do
  !ann <- fromBencoding =<< bencDictLookup "announce" bd
  !ann_list <- fromBencoding =<< case bencDictLookup "announce-list" bd of
                               Just xs -> Just xs
                               Nothing -> Just $ BencList []
  let !trackers = ann : (mapMaybe (fromBencoding . head) $  mapMaybe fromBencoding ann_list)
  let !creatdate = fromBencoding =<< bencDictLookup "creation date" bd
  let !comment = fromBencoding =<< bencDictLookup "comment" bd
  let !creatby = fromBencoding =<< bencDictLookup "created by" bd
  infoDict <- bencDictLookup "info" bd
  !pieceLength <- fromBencoding  =<< bencDictLookup "piece length" infoDict
  pieces <- fromBencoding =<< bencDictLookup "pieces" infoDict
  let !piecesList = breakList 20 pieces
  let !priv = fromBencoding =<< bencDictLookup "private" infoDict
  !name <- fromBencoding =<< bencDictLookup "name" infoDict
  files <- case bencDictLookup "files" infoDict of
             Just xs -> getFilesList xs          -- multifile torrent
             Nothing -> do                       -- single file torrent
               len <- fromBencoding =<< bencDictLookup "length" infoDict
               let md5 = fromBencoding =<< bencDictLookup "md5sum" infoDict
               return [(len, md5, [name])]
  -- While the next line is far from optimal but it gets the job done
  -- with little cost.
  let !infoHash = L.pack . sha1 . encodeString $ infoDict 
  return $ emptyMetainfo {
               getTrackers = trackers
             , getCreationDate = creatdate
             , getComment = comment
             , getCreatedBy = creatby
             , getPieceLength = pieceLength
             , getPieceList = piecesList
             , getPrivate = priv
             , getName = name
             , getFileList = files
             , getInfoHash = infoHash
             }
    where
      getFilesList :: Bencoding -> Maybe [(Integer, Maybe String, [String])]
      getFilesList xs = case fromBencoding xs of
                          Nothing -> Nothing
                          Just ys -> sequence . map getFileTriplet $ ys
      getFileTriplet :: Bencoding -> Maybe (Integer, Maybe String, [String])
      getFileTriplet bd = do
        len <- fromBencoding =<< bencDictLookup "length" bd
        let md5 = fromBencoding =<< bencDictLookup "md5sum" bd
        path <- (sequence . map fromBencoding) =<< fromBencoding =<< bencDictLookup "path" bd
        return (len, md5, path)

breakList :: Int -> L.ByteString -> [L.ByteString]
breakList n = go (fromIntegral n) []
    where
      go n' !t xs
          | L.null xs = reverse t
          | otherwise = let (a,b) = L.splitAt n' xs
                        in go n' (a:t) b
