-- |
-- Module      :  Data.PeerId
-- Copyright   :  (c) Alexandru Scvortov 2008
-- License     :  LGPL (see LICENSE file)
-- Maintainer  :  scvalex@gmail.com
--

module Data.PeerId
    ( PeerId
    , makeMyPeerId
    , translatePeerId
    ) where

import Text.Printf ( printf )
import Data.Extra ( randdigit )
import Text.Regex.Simple ( matchPattern )

type PeerId = String

-- | generate a peerid for this client
--
-- We're using Azureus style encoding: '-', two characters for client
-- id, four ascii digits for version number, '-', followed by random
-- numbers.
--
-- So, one valid peerid might be "-ZQ0000-123456654321"
makeMyPeerId :: IO PeerId
makeMyPeerId = do
  randString <- sequence $ replicate 12 randdigit
  return $ printf "-ZQ0000-%s" randString

-- | Match a peerid against an internal table and return the peer's
-- | name.  You say "-AZ1234-010101010101" and I answer "Azureus".  If
-- | the peerid is not recognized, will return "Unknown" (ie this
-- | function never fails).
translatePeerId :: PeerId -> String
translatePeerId pid = snd . head . filter (\(mat, _) -> mat pid) $ clientsTable

clientsTable :: [(PeerId -> Bool, String)]
clientsTable = concat [ map (\(p, n) -> (matchPattern ("-" ++ p ++ "....-............"), n)) azureusStyleClients
                      , map (\(p, n) -> (matchPattern (p ++ "..................."), n)) shadowStyleClients
                      , oddPeers
                      , [(matchPattern "....................", "Unrecognized")] -- this should match ANY 20 byte string
                      ]

-- Peers that don't use any of the common naming schemes.
oddPeers :: [(PeerId -> Bool, String)]
oddPeers =  [ (matchPattern "XBT.................", "XBT Client")
            , (matchPattern "OP..................", "Opera")
            , (matchPattern "-ML.................", "MLdonkey")
            , (matchPattern "-BOW................", "Bits on Wheels")
            , (matchPattern "Q...................", "Queen Bee")
            , (matchPattern "AZ2500BT............", "BitTyrant")
            , (matchPattern "346.................", "TorrenTopia")
            , (matchPattern "..RS................", "Rufus")
            , (matchPattern "-G3.................", "G3 Torrent")
            , (matchPattern "-FG.................", "FlashGet")
            , (matchPattern "-NE.................", "BT Next Evolution")
            , (matchPattern "AP..................", "AllPeers")
            , (matchPattern "QVOD................", "Qvod")
            ]

-- list taken from
-- http://wiki.theory.org/BitTorrentSpecification#peer_id
azureusStyleClients :: [(String, String)]
azureusStyleClients = [ ("AG", "Ares")
                      , ("A~", "Ares")
                      , ("AR", "Arctic")
                      , ("AT", "Artemis")
                      , ("AX", "BitPump")
                      , ("AZ", "Azureus")
                      , ("BB", "BitBuddy")
                      , ("BC", "BitComet")
                      , ("BF", "Bitflu")
                      , ("BG", "BTG (uses Rasterbar libtorrent)")
                      , ("BP", "BitTorrent Pro (Azureus + spyware)")
                      , ("BR", "BitRocket")
                      , ("BS", "BTSlave")
                      , ("BW", "BitWombat")
                      , ("BX", "~Bittorrent X")
                      , ("CD", "Enhanced CTorrent")
                      , ("CT", "CTorrent")
                      , ("DE", "DelugeTorrent")
                      , ("DP", "Propagate Data Client")
                      , ("EB", "EBit")
                      , ("ES", "electric sheep")
                      , ("FC", "FileCroc")
                      , ("FT", "FoxTorrent")
                      , ("GS", "GSTorrent")
                      , ("HL", "Halite")
                      , ("HN", "Hydranode")
                      , ("KG", "KGet")
                      , ("KT", "KTorrent")
                      , ("LC", "LeechCraft")
                      , ("LH", "LH-ABC")
                      , ("LP", "Lphant")
                      , ("LT", "libtorrent")
                      , ("lt", "libTorrent")
                      , ("LW", "LimeWire")
                      , ("MO", "MonoTorrent")
                      , ("MP", "MooPolice")
                      , ("MR", "Miro")
                      , ("MT", "MoonlightTorrent")
                      , ("NX", "Net Transport")
                      , ("OT", "OmegaTorrent")
                      , ("PD", "Pando")
                      , ("qB", "qBittorrent")
                      , ("QD", "QQDownload")
                      , ("QT", "Qt 4 Torrent example")
                      , ("RT", "Retriever")
                      , ("S~", "Shareaza alpha/beta")
                      , ("SB", "~Swiftbit")
                      , ("SS", "SwarmScope")
                      , ("ST", "SymTorrent")
                      , ("st", "sharktorrent")
                      , ("SZ", "Shareaza")
                      , ("TN", "TorrentDotNET")
                      , ("TR", "Transmission")
                      , ("TS", "Torrentstorm")
                      , ("TT", "TuoTu")
                      , ("UL", "uLeecher!")
                      , ("UT", "uTorrent")
                      , ("VG", "Vagaa")
                      , ("WT", "BitLet")
                      , ("WY", "FireTorrent")
                      , ("XL", "Xunlei")
                      , ("XT", "XanTorrent")
                      , ("XX", "Xtorrent")
                      , ("ZT", "ZipTorren")
                      ]

-- list taken from
-- http://wiki.theory.org/BitTorrentSpecification#peer_id
shadowStyleClients :: [(String, String)]
shadowStyleClients = [ ("A", "ABC")
                     , ("O", "Osprey Permaseed")
                     , ("Q", "BTQueue")
                     , ("R", "Tribler")
                     , ("S", "Shadow's")
                     , ("T", "BitTornado")
                     , ("U", "UPnP NAT Bit Torrent")
                     ]
