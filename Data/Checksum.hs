-- |
-- Module      :  Data.Checksum
-- Copyright   :  (c) Alexandru Scvortov 2009
-- License     :  LGPL (see LICENSE file)
-- Maintainer  :  scvalex@gmail.com
--

module Data.Checksum 
    ( sha1
    , hexGroupsToBytes
    , bytesToHexGroups
    ) where

-- FIXME: Remove dependency on Crypto.

import qualified Data.ByteString.Lazy.Char8 as L
import Data.ByteString.Extra
import Data.Char
import Data.Digest.SHA1
import Data.Extra
import Data.Word
import Numeric

c2w :: Char -> Word8
c2w = fromIntegral . ord

w2c :: Word8 -> Char
w2c = chr . fromIntegral

s2wl :: String -> [Word8]
s2wl = map c2w

-- FIXME: Use IsByteString in sha1.

-- | Compute the SHA1 checksum of s and return it as a 40 character 'String'.
sha1 :: String -> String
sha1 s = padFront 40 '0' hashedhex
    where
      hashedhex :: String
      hashedhex = showHex hashed ""
      hashed :: Integer
      hashed = Data.Digest.SHA1.toInteger $ hash $ s2wl s

-- | Convert a pretty-printed hex number (with 16 bit digits) to a
-- binary one.  Useful when used in conjunction with
-- 'bytesToHexGroups'.  Used to manipulate the output of 'sha1'.
hexGroupsToBytes :: (IsByteString a, IsByteString b) => a -> b
hexGroupsToBytes = fromByteString . go L.empty . toByteString
    where
      go acc s 
         | L.null s  = acc
         | otherwise = let (h, t) = L.splitAt 2 s
                           c = chr . fst . head . readHex . L.unpack $ h
                       in go (acc `L.snoc` c) t

-- FIXME: bytesToHexGroups is ugly.
-- | Convert a binary hex number to a pretty-printed one (16 bit
-- digits).  Useful when used in conjunction with
-- 'bytesToHexGroups'. Used to manipulate the output of 'sha1'.
bytesToHexGroups :: (IsByteString a, IsByteString b) => a -> b
bytesToHexGroups = fromByteString . go L.empty . toByteString
    where
      go acc s 
         | L.null s  = acc
         | otherwise = let (h, t) = L.splitAt 1 s
                           gr = L.pack $ reverse $ take 2 $ reverse 
                                $ showString "00"
                                . (showHex . ord . head . L.unpack $ h) $ ""
                       in go (acc `L.append` gr) t
