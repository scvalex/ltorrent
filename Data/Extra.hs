{-# LANGUAGE MultiParamTypeClasses, FunctionalDependencies, FlexibleInstances #-}

-- |
-- Module      :  Data.Extra
-- Copyright   :  (c) Alexandru Scvortov 2009
-- License     :  LGPL (see LICENSE file)
-- Maintainer  :  scvalex@gmail.com
--

module Data.Extra
    ( merge
    , randint
    , randdigit
    , takeRandom
    , Paddable(..)
    , makeGroupedSum, mgsConfigTime, mgsConfigDiskSpace
    , showPrettyFilesize, showPrettyTime
    ) where

import qualified Data.ByteString.Lazy.Char8 as L
import Data.Char ( chr, ord )
import Data.List (nub)
import System.Random (getStdRandom, randomR)
import Numeric ( readHex, showHex )

-- | Merge two SORTED lists discarding duplicates.
merge :: Ord a => [a] -> [a] -> [a]
merge xs ys = nub $ go [] xs ys
    where
      go acc xs [] = reverse acc ++ xs
      go acc [] ys = reverse acc ++ ys
      go acc allx@(x:xs) ally@(y:ys)
          | x <= y    = go (x:acc) xs ally
          | otherwise = go (y:acc) allx ys

-- FIXME: Try using MonadRandom (see Hackage) instead of getStdRandom
-- and co. in the Data.Extra functions.

-- | Return an int from inclusive range.
randint :: (Int, Int) -> IO Int
randint (l, u) = getStdRandom (randomR (l, u))

-- | Return a random digit.
randdigit :: IO Char
randdigit = getStdRandom (randomR ('0', '9'))

-- | Return a random element from a list.
takeRandom :: [a] -> IO (Maybe a)
takeRandom xs = do
  if length xs > 0
     then do
       i <- randint (0, length xs - 1)
       return $ Just $ xs !! i
     else
       return Nothing

-- FIXME; Is class Paddable really necessary,

class Paddable s c | s -> c where
    padFront :: Int -> c -> s -> s
    padBack  :: Int -> c -> s -> s

instance Paddable L.ByteString Char where
    padFront n c s = let l = fromIntegral . L.length $ s
                     in if l >= n
                        then s
                        else L.pack (replicate (fromIntegral (n-l)) c) `L.append` s
    padBack n c s  = let l = fromIntegral . L.length $ s
                     in if l >= n
                        then s
                        else s `L.append` L.pack (replicate (fromIntegral (n-l)) c)

instance Paddable [a] a where
    padFront n c s = let l = length s
                     in if l >= n
                        then s
                        else replicate (n-l) c ++ s
    padBack n c s  = let l = length s
                     in if l >= n
                        then s
                        else s ++ replicate (n-l) c

mgsConfigDiskSpace = [ (1024000000, "GB")
                     , (1024000, "MB")
                     , (1024, "KB")
                     , (1, "B") ]

mgsConfigTime = [ (86400, "d")
                , (3600, "h")
                , (60, "min")
                , (1, "s") ]

-- | Perform conversions like 1536 -> "1Kb 512b" or 367 -> "6m 7s".
makeGroupedSum :: [(Integer, String)] -> Integer -> [(Integer, String)]
makeGroupedSum = go [] 0
    where
      go acc cur ((i, s):_) 0        = reverse $ upAcc acc cur s
      go acc cur os@((i, s):ss) left = if i <= left
                                then go acc (cur+1) os (left-i)
                                else go (upAcc acc cur s) 0 ss left
      upAcc acc 0 _   = acc
      upAcc acc cur s = (cur, s) : acc

showPrettyFilesize = unwords . map (\(i, s) -> show i ++ s) . take 2 . makeGroupedSum mgsConfigDiskSpace
showPrettyTime = tryTail . concat . map (\(i, _) -> ':' : show i) . makeGroupedSum mgsConfigTime
    where
      tryTail :: [a] -> [a]
      tryTail [] = []
      tryTail xs = tail xs
