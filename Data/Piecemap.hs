-- |
-- Module      :  Data.Piecemap
-- Copyright   :  (c) Alexandru Scvortov 2009
-- License     :  LGPL (see LICENSE file)
-- Maintainer  :  scvalex@gmail.com
--
-- Holds ways of representing the set of pieces a client has.  Network
-- peers usually reply with 'Piecemap' while 'PieceStatusArray' is
-- used internally.
--

module Data.Piecemap
    ( Piecemap, makePiecemap
    ) where

import Data.Array.Unboxed
import Data.Bits
import qualified Data.ByteString.Lazy.Char8 as L
import Data.Char

-- | What pieces does a client have?
type Piecemap = UArray Int Bool

-- | Convert a lazy ByteString into a 'Piecemap'.
makePiecemap :: L.ByteString -> Piecemap
makePiecemap bs = listArray (0, 8*len) . concatMap charToBools . L.unpack $ bs
    where
      len = fromIntegral . L.length $ bs

charToBools :: Char -> [Bool]
charToBools c = let c' = fromEnum c
                in map (testBit c') [7, 6..0]
