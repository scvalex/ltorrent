{-# LANGUAGE TypeSynonymInstances, FlexibleInstances, UndecidableInstances #-}

-- |
-- Module      :  Data.ByteString.Extra
-- Copyright   :  (c) Alexandru Scvortov 2009
-- License     :  LGPL (see LICENSE file)
-- Maintainer  :  scvalex@gmail.com
--
-- Contains the IsByteString class that simplifies the translation of
-- ByteString-like datatypes to and from ByteString.

module Data.ByteString.Extra
    ( IsByteString(..)
    , Monoid(..) -- this is more for convenience than for other reasons
    ) where

import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy.Char8 as L
import Data.Int
import Data.Monoid

class IsByteString a where
    fromByteString :: L.ByteString -> a
    toByteString :: a -> L.ByteString

instance IsByteString L.ByteString where
    fromByteString = id
    toByteString = id

instance IsByteString Char where
    fromByteString = L.head
    toByteString = L.singleton

instance IsByteString String where
    fromByteString = L.unpack
    toByteString = L.pack

instance IsByteString B.ByteString where
    fromByteString = B.concat . L.toChunks
    toByteString = L.fromChunks . (:[])

{-
instance (Num a, Show a, Read a) => IsByteString a where
    fromByteString = read . L.unpack
    toByteString = L.pack . show 
-}
