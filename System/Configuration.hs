-- |
-- Module      :  System.Configuration
-- Copyright   :  (c) Alexandru Scvortov 2009
-- License     :  LGPL (see LICENSE file)
-- Maintainer  :  scvalex@gmail.com
--

-- FIXME: System.Configuration keys depend on what the file system
-- accepts as a valid path.  Neeless to say, this should not be the
-- case.

module System.Configuration
    ( getLTorrentDir
    , readConfig, writeConfig, writeConfigIfMissing
    , readConfigDefault
    ) where

import Control.Exception.Extra
import Control.Monad.Trans
import Data.ByteString.Extra
import qualified Data.ByteString.Lazy.Char8 as L
import Data.List ( foldl' )
import System.Directory ( getAppUserDataDirectory, doesFileExist )
import System.Directory.Extra
import System.IO

-- | Return the path to @~\/.ltorrent@.
getLTorrentDir :: MonadIO m => m FilePath
getLTorrentDir = liftIO $ getAppUserDataDirectory "ltorrent"

-- Ensures that all necessary directories exist and returns the
-- keypath.
sanitizeKeyPath :: [String] -> IO FilePath
sanitizeKeyPath ks = do
  ltd <- getLTorrentDir
  let fp = foldl' (</>) ltd ("config" : (init ks))
  createFilePathIfMissing fp
  return $ fp </> last ks

-- | Write a (key, value) pair to the configuration.  Warning: may
-- | result in exception.
writeConfig :: (IsByteString a) => [String] -> a -> IO ()
writeConfig ks v = do
  k <- sanitizeKeyPath ks
  withBinaryFile k WriteMode $ \h -> do
          L.hPut h (toByteString v)
          hFlush h

-- | Write a (key, value) pair to the configuration ONLY if the key
-- | has not already been used.
-- |
-- | Returns False is the key is present or True if it has been added.
writeConfigIfMissing :: (IsByteString a) => [String] -> a -> IO Bool
writeConfigIfMissing ks v = do
  ltd <- getLTorrentDir
  e <- doesFileExist $ foldl' (</>) ltd ("config":ks)
  if e
     then return False
     else writeConfig ks v >> return True

-- | Read the value of a key from the configuration.  Warning: may
-- | result in exception.
readConfig :: (IsByteString a) => [String] ->  IO a
readConfig ks = do
  ltd <- getLTorrentDir
  let k = foldl' (</>) ltd ("config":ks)
  e <- doesFileExist k
  if not e
     then fail "key not found"
     else do
       withBinaryFile k ReadMode $ \h -> do
                  txt <- L.hGetContents h
                  if L.length txt > 0
                    then return $ fromByteString txt
                    else fail "I am depressed"

-- | Attempt to read a key's value.  On succes, return the value but
-- | on failure, return the default value provided.
readConfigDefault :: (IsByteString a) => [String] -> a -> IO a
readConfigDefault ks dflt = readConfig ks `catchAll` (const $ return dflt)
