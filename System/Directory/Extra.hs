-- |
-- Module      :  System.Directory.Extra
-- Copyright   :  (c) Alexandru Scvortov 2008
-- License     :  LGPL (see LICENSE file)
-- Maintainer  :  scvalex@gmail.com
--

module System.Directory.Extra
    ( createFilePathIfMissing
    , (</>)
    , calculateFileChecksum
    ) where

import Control.Monad ( when )
import qualified Data.ByteString.Lazy.Char8 as L
import Data.Checksum
import System.Directory
import System.FilePath.Posix

-- | Ensure that the path specified exists and is a directory.
-- Returns whether the directory was created.
createFilePathIfMissing :: FilePath -> IO Bool
createFilePathIfMissing fp = do
  f <- doesFileExist fp
  when f $ removeFile fp
  e <- doesDirectoryExist fp
  createDirectoryIfMissing True fp
  return $ not e

-- | Calculate the SHA1 of a file's contents.
calculateFileChecksum :: FilePath -> IO L.ByteString
calculateFileChecksum fp = do 
  txt <- readFile fp
  return . hexGroupsToBytes . L.pack . sha1 $ txt
