-- |
-- Module      :  BitTorrent.Peer
-- Copyright   :  (c) Alexandru Scvortov 2009
-- License     :  LGPL (see LICENSE file)
-- Maintainer  :  scvalex@gmail.com
--
-- I'm using this page as a reference
-- http://wiki.theory.org/BitTorrentSpecification
--
-- Thanks to:
--   A warm thank you to the people behing Conjure!
--

module BitTorrent.Peer
    ( Net.Entity(..)
    , Peer(..), newPeerIO, newPeerSTM
    , setPeerChocked, setPeerInterested
    , setPiecemap, readPiecemap
    , Handshake(..), hPutHandshake, hGetHandshake
    , Message(..), hGetMessage, hPutMessage
    )
    where

import GHC.Conc
import Control.Monad
import Data.Bencoding
import Data.Bits
import qualified Data.ByteString.Lazy.Char8 as L
import qualified Data.ByteString.Internal as BI
import qualified Data.ByteString as B
import Data.ByteString.Extra
import Data.Checksum
import Data.Function ( on )
import Data.List
import Data.PeerId
import Data.Piecemap
import Data.Word
import Foreign.ForeignPtr
import Foreign.Marshal.Array
import qualified Network.Entity as Net
import System.IO
import Text.Printf ( printf )

data Peer = Peer
          { getPeerIP :: String
          , getPeerPort :: Int
          , getIsEngaged :: Bool -- ^ are we connected to it now?
          -- ^ getIsEngaged is set by peerManager before assigning the
          -- peer to a handler and is unset after the handler exits.

          , getFlags :: TVar String
          -- ^ getFlags is a String that acts as a set.
          --   * 'C' means we are choking
          --   * 'c' means we are being choked
          --   * 'I' means we are interested
          --   * 'i' means the peer is interested
          -- getFlags is initially "Cc"
          -- Note: this needs to be a TVar because it is modified by
          -- peer handler which cannot modify the Peer.

          , getPiecemap :: TVar (Maybe Piecemap)
          -- ^ Note: this needs to be a TVar because it is modified by
          -- peer handler which cannot modify the Peer.

          , getClient :: TVar String
          -- ^ readable client id; something like uTorrent
          }

instance BencodedData Peer where
    fromBencoding = undefined -- FIXME: add fromBencoding to Peer
    toBencoding p = bencDictInsert ("ip", getPeerIP p)
                  $ bencDictInsert ("port", ((0 :: Integer)+) . fromIntegral . getPeerPort $ p)
                  $ bencDictInsert ("engaged", if getIsEngaged p then "true" else "false")
                  $ newBencoding

instance Net.Entity Peer where
    getIP = getPeerIP
    getPort = getPeerPort

instance Eq Peer where
    x == y = on (==) getPeerIP x y && on (==) getPeerPort x y

instance Ord Peer where
    compare = compare `on` Net.getIP

instance Show Peer where
    show p = printf "Peer { ip: %s:%d, engaged: %s, flags: '???' }"
                    (Net.getIP p) (Net.getPort p) (if getIsEngaged p then "yes" else "no")

newPeerSTM :: String -> Int -> STM Peer
newPeerSTM ip port = do
  fs <- newTVar "Cc"
  pm <- newTVar Nothing
  gc <- newTVar "Not responding"
  return $ Peer ip port False fs pm gc

-- | newPeerIO ip port returns a new Peer with specified ip and port.
newPeerIO :: String -> Int -> IO Peer
newPeerIO i p = atomically (newPeerSTM i p)

setPeerChocked :: Peer -> Bool -> IO Peer
setPeerChocked = setFlag 'c'

setPeerInterested :: Peer -> Bool -> IO Peer
setPeerInterested = setFlag 'i'

setFlag :: Char -> Peer -> Bool -> IO Peer
setFlag c p b = do
  fs <- atomically . readTVar . getFlags $ p
  if b
     then when (c `notElem` fs) $ atomically . writeTVar (getFlags p) $ c:fs
     else when (c `elem` fs) $ atomically . writeTVar (getFlags p) $ fs \\ [c]
  return p

setPiecemap :: Peer -> Piecemap -> IO Peer
setPiecemap p pm = do
  atomically . writeTVar (getPiecemap p) . Just $ pm
  return p

readPiecemap :: Peer -> IO (Maybe Piecemap)
readPiecemap = atomically . readTVar . getPiecemap

-- | a handshake is the first thing peers exchange
data Handshake = Handshake
    { getHandshakeInfoHash :: L.ByteString -- ^ infohash 
    , getHandshakePeerId :: PeerId -- ^ peerid
    } deriving (Show, Eq)

data Message = KeepAlive 
             | Choke
             | Unchoke
             | Interested
             | NotInterested
             | Have !Int
             | BitField L.ByteString
             | Request !Int !Int !Int
             | RequestedPiece !Int !Int L.ByteString
             | Cancel !Int !Int !Int
             | Disconnect String -- | extension to protocol
             | Unknown !Int
               deriving (Eq, Show)

-- | sends the handshake to the peer; this is how a conversation
-- should start
hPutHandshake :: Handle -> Handshake -> IO ()
hPutHandshake h (Handshake ih pid) = do
  hPutStr h $ "\19BitTorrent protocol" ++ replicate 8 '\0'
  L.hPut h $ hexGroupsToBytes ih
  L.hPut h (toByteString pid)
  hFlush h

hGetHandshake :: Handle -> IO Handshake
hGetHandshake h = do
  c <- hGetChar h
--  putStrLn "getting handshake"
  let len = fromEnum c
  if len /= 19 
    then fail "unknown protocol in handshake"
    else do
      pro <- L.hGet h len
      if pro /= toByteString "BitTorrent protocol"
        then fail "unkown protocol in handshake"
        else do
          L.hGet h 8
          ih <- L.hGet h 20
          pid <- L.hGet h 20 -- TODO this might be wrong
          return $ Handshake ih (fromByteString pid)

-- TODO this seems overly complicated
hGetMessage :: Handle -> IO Message    
hGetMessage h = do
  len <- hGetInt h
  if len == 0 
    then return KeepAlive
    else do
      let loop l ptr = do 
                  hWaitForInput h (-1)
                  realLen <- hGetBufNonBlocking h ptr l
                  if realLen == l
                     then return ()
                     else loop (l-realLen) (advancePtr ptr realLen)
      msgID <- (return . fromEnum . L.head) =<< L.hGet h 1
      msg <- if msgID == 7
             then do
               fp <- mallocForeignPtrBytes (len-1)
               withForeignPtr fp $ loop (len-1)
               return $ L.fromChunks [BI.fromForeignPtr fp 0 (len-1)]
             else L.hGet h (len-1)
      return $! parseMessage msgID msg

parseMessage :: Int -> L.ByteString -> Message
parseMessage msgID payload 
    = case msgID of
        0 -> Choke
        1 -> Unchoke
        2 -> Interested
        3 -> NotInterested
        4 -> Have (fst $ parseInt payload) -- it's a bigendian
        5 -> BitField payload
        6 -> let [idx, offset, len] = take 3 (combine parseInt)
             in Request idx offset len
        7 -> let (idx, str') = parseInt payload
                 (offset, str'') = parseInt str'
             in RequestedPiece idx offset str''
        8 -> let [idx, offset, len] = take 3 (combine parseInt)
             in Cancel idx offset len
        _ -> Unknown msgID
    where
      combine f = let loop str = let (val, str') = f str
                                 in val : loop str'
                  in loop payload

hGetInt :: Handle -> IO Int
hGetInt h = do
  [a, b, c, d] <- mapM (liftM fromEnum) (replicate 4 (hGetChar h))
  return (a `shiftL` 24 + b `shiftL` 16 + c `shiftL` 8 + d)

hPutInt :: Handle -> Int -> IO ()
hPutInt h = B.hPut h . B.pack . serializeInt

parseInt :: L.ByteString -> (Int, L.ByteString)
parseInt s = let [a, b, c, d] = map (fromEnum . L.index s) [0..3]
             in (sum $ zipWith shiftL [a, b, c, d] [24, 16, 8, 0], L.drop 4 s)

-- | write a message to a handle
hPutMessage :: Handle -> Message -> IO ()
hPutMessage h msg = do
  let str = serializeMessage msg
  hPutInt h (myLength str)
  L.hPut h str
  hFlush h
      where
        myLength = sum . map B.length . L.toChunks -- TODO why not just use L.length?

serializeMessage :: Message -> L.ByteString
serializeMessage KeepAlive = L.empty
serializeMessage msg = L.singleton tag `L.append` rest
    where
      (tag, rest) = case msg of
            Choke         -> ('\0', L.empty)
            Unchoke       -> ('\1', L.empty)
            Interested    -> ('\2', L.empty)
            NotInterested -> ('\3', L.empty)
            Have i        -> ('\4', strictToLazy . B.pack . serializeInt $ i)
            BitField pieces -> ('\5', pieces) -- TODO I don't think 
                                              -- this is ever used
            Request i off size -> ('\6', strictToLazy . B.pack 
                                        . concatMap serializeInt $ [i, off, size])
            RequestedPiece i off str -> ('\7', (strictToLazy . B.pack 
                                                  . concatMap serializeInt $ [i,off]) 
                                         `L.append` str)
            Cancel i off size -> ('\8', strictToLazy . B.pack 
                                       . concatMap serializeInt $ [i, off, size])
            _ -> error "Veird."
      strictToLazy :: B.ByteString -> L.ByteString
      strictToLazy = L.fromChunks . (:[])

-- NOTE this might have the bytes reversed
serializeInt :: Int -> [Word8]
serializeInt x = [ fromIntegral $ x `shiftR` shft .&. 255 | shft <- [24, 16, 8, 0] ]
