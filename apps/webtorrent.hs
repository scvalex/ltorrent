module Main where

import BitTorrent
import Control.Concurrent
import Control.Concurrent.Extra
import Control.Concurrent.MVar
import Control.Concurrent.STM
import Control.Exception.Extra
import Control.ITC
import Control.Monad
import Data.Bencoding
import qualified Data.ByteString.Lazy.Char8 as L
import Data.Extra
import Data.List
import Data.Maybe
import Data.Time.Clock
import LTorrent
import Network.HTTP hiding ( port )
import Network.HTTP.SimpleWebServer
import Network.URI
import System.Environment ( getArgs )
import System.IO
import Text.JSON
import Text.JSON.Bencoding
import Text.Printf ( printf )

debugMode = True
port = 8000

startWebServer :: TChan BridgeMessage -> IO ()
startWebServer inCh = do
  printf "Starting webTorrent on port %d\n" port
  runHttpServer port (pathSplitterHandler [ ("/hello", helloHandler)
                                          , ("/torrents", torrentsHandler inCh)
                                          , ("/torrent/", torrentHandler inCh)
                                          , ("/static", staticDirHandler "./" inCh)
                                          , ("/lp", staticDirHandler "web/livepipe/src/" inCh)
                                          , ("/pt", staticDirHandler "web/prototype/src/" inCh)
                                          , ("/sc", staticDirHandler "web/scriptaculous/src/" inCh)
                                          , ("/stat", statHandler inCh)
                                          , ("/", staticPageHandler "web/webtorrent.html" inCh) ])

main :: IO ()
main = do
    inCh <- startLTorrent
    putStrLn "lTorrent ready"

    forkIOThrowErrorsHere (startWebServer inCh)

    fs <- getArgs
    attemptStartTorrents inCh fs

    forever $ threadDelay 6000000

-- FIXME Move some of the more generic RequestHandlers to their own
-- module.

-- FIXME staticDirHandler should use ByteStrings and should be able to
-- guess the MIME of a file.

staticPageHandler :: String -> TChan BridgeMessage -> RequestHandler
staticPageHandler f _ _ = sendFile f

staticDirHandler :: String -> TChan BridgeMessage -> RequestHandler
staticDirHandler pre inCh req = do
  let path = pre ++ (tail . uriPath . rqURI $ req)
  sendFile path `catchAll` (\e -> putStrLn ("WARNING: " ++ show e) >>
                               (return $ notFoundResponse [] $ show e))

sendFile p = do
  txt <- readFile p
  case ".html" `isSuffixOf` p of
    False -> return $ okResponse [] txt
    True  -> return $ htmlResponse txt

statHandler :: TChan BridgeMessage -> RequestHandler
statHandler inCh _ = do
  resp <- takeMVar =<< sendNOP inCh
  return $ jsonResponse $ encodeStrict $ showJSON resp

helloHandler :: RequestHandler
helloHandler _ = return $ okResponse [] "Hello World\n"

torrentsHandler :: TChan BridgeMessage -> RequestHandler
torrentsHandler inCh _ = do
  tis <- takeMVar =<< sendGetTorrentList inCh
  return $ jsonResponse $ encodeStrict $ showJSON tis

-- FIXME: torrentHandler should probably resuse pathSplitterHandler
-- instead of manually matching the commands.
torrentHandler :: TChan BridgeMessage -> RequestHandler
torrentHandler inCh req = do
  let urip = uriPath . rqURI $ req
  let ih = toByteString . takeWhile (/='/') $ urip
  let comm = tail $ case dropWhile (/='/') $ urip of
                      [] -> "/all"
                      s -> s
  doCommand urip comm
      where
        doCommand urip c 
            | c == "all" = do
                    tis <- takeMVar =<< sendGetTorrentInfo inCh ih
                    return $ jsonResponse $ encodeStrict $ showJSON tis
            | c == "piecestatus" = do
                    ps <- takeMVar =<< sendGetPieceArray inCh ih
                    return $ jsonResponse $ encodeStrict $ showJSON ps
            | c == "trackers" = do
                    ts <- takeMVar =<< sendGetTrackers inCh ih        
                    return $ jsonResponse $ encodeStrict $ showJSON ts
            | c == "peers" = do
                    ps <- takeMVar =<< sendGetPeers inCh ih
                    return $ jsonResponse $ encodeStrict $ showJSON ps
            | otherwise = do
                    printf "WARNING: Unknown command %s\n" c
                    return $ okResponse [] $ printf "Damnation!\n%s\n" c
            where
              ih = toByteString . takeWhile (/='/') $ urip

-- Match the given path with a handler, then invoke the handler with
-- the path STRIPPED of the matched prefix.
pathSplitterHandler :: [(String, RequestHandler)] -> RequestHandler
pathSplitterHandler hs req = let path = uriPath $ rqURI req
                                 phs = filter (\(s, _) -> s `isPrefixOf` path) hs
                                 invokeHandler h s =
                                   let (Just path') = s `stripPrefix` path
                                       newURI = (rqURI req) {uriPath = path'}
                                   in h (req {rqURI = newURI})
                             in case phs of
                                  [] -> do
                                       putStrLn $ "WARNING: No handler found for " ++ path
                                       return $ notFoundResponse [] "Not Found\n"
                                  [(s, h)] -> do
                                       putStrLn $ "NOTE: Single handler invoked for " ++ path
                                       invokeHandler h s
                                  ((s, h):_) -> do
                                       putStrLn $ "WARNING: More than one handler found for " ++ path
                                       invokeHandler h s

attemptStartTorrents :: TChan BridgeMessage -> [FilePath] -> IO [Metainfo]
attemptStartTorrents inCh fs = do
  mms <- mapM (attemptStartTorrent inCh) fs
  return $ catMaybes mms

attemptStartTorrent :: TChan BridgeMessage -> FilePath -> IO (Maybe Metainfo)
attemptStartTorrent inCh fp = do
  doStart `catchAll` (\e -> print e >> return Nothing)
      where
        doStart = do
            mi <- readMetainfo fp
            withBinaryFile fp ReadMode $ \h -> do
                bs <- L.hGetContents h
                o <- sendDownloadTorrent inCh bs
                resp <- takeMVar o
                let (Just err) = fromBencoding =<< bencDictLookup "error" resp
                if not . L.null $ err
                   then fail $ fromByteString err
                   else return $ Just mi

-- FIXME: extractResponses looks like it could use a few Arrows
extractResponses :: [(Bencoding, Metainfo)] -> [(String, Metainfo)]
extractResponses = catMaybes . map extractResponse
    where
      extractResponse (be, mi) = do
        err <- fromBencoding =<< bencDictLookup "error" be
        if not . L.null $ err
           then fail $ printf "ERROR: Command resulted in error: %s\n" (L.unpack err)
           else case fromBencoding =<< bencDictLookup "response" be of
                  Nothing -> fail "ERROR: could not extract response"
                  Just resp -> return (resp, mi)
