module Main where

import BitTorrent 
import Control.Concurrent
import Control.Concurrent.MVar
import Control.Concurrent.STM
import Control.Exception.Extra
import Control.ITC
import Control.Monad
import Data.Bencoding
import qualified Data.ByteString.Lazy.Char8 as L
import Data.Extra
import Data.Maybe
import Data.Time.Clock
import LTorrent
import System.Environment ( getArgs )
import System.IO
import Text.Printf ( printf )

-- FIXME: See if you can use Jeff Heard's Control.Concurrent.Future in
-- place of the fokrIO...resp <- takeMVar o... sequences.

debugMode = False

main :: IO ()
main = do
    fs <- getArgs
    inCh <- startLTorrent
    putStrLn "lTorrent ready"

    mis <- attemptStartTorrents inCh fs

    putStrLn "lTorrent downloading:"
    mapM_ (printf "\t%s\n" . getName) mis

    iTime <- getCurrentTime
    let mainLoop i t = do
          t' <- getCurrentTime
          os <- mapM (sendGetPieceArray inCh . getInfoHash) mis
          resps <- mapM takeMVar os
          let ss = extractResponses $ zip resps mis
          mapM_ (printBar i (iTime, t)) ss
          threadDelay (400000) >> mainLoop (i+1) t'
    mainLoop 0 iTime

    forever $ do
         sendNOP inCh
         threadDelay (10^6)

    forever $ threadDelay 6000000

attemptStartTorrents :: TChan BridgeMessage -> [FilePath] -> IO [Metainfo]
attemptStartTorrents inCh fs = do
  mms <- mapM (attemptStartTorrent inCh) fs
  return $ catMaybes mms

attemptStartTorrent :: TChan BridgeMessage -> FilePath -> IO (Maybe Metainfo)
attemptStartTorrent inCh fp = do
  doStart `catchAll` (\e -> print e >> return Nothing)
      where
        doStart = do
            mi <- readMetainfo fp
            bs <- L.readFile fp
            o <- sendDownloadTorrent inCh bs
            resp <- takeMVar o
            let (Just err) = fromBencoding =<< bencDictLookup "error" resp
            if not . L.null $ err
               then fail $ fromByteString err
               else return $ Just mi

-- FIXME: extractResponses looks like it could use a few Arrows
extractResponses :: [(Bencoding, Metainfo)] -> [(String, Metainfo)]
extractResponses = catMaybes . map extractResponse
    where
      extractResponse (be, mi) = do
        err <- fromBencoding =<< bencDictLookup "error" be
        if not . L.null $ err
           then fail $ printf "ERROR: Command resulted in error: %s\n" (L.unpack err)
           else case fromBencoding =<< bencDictLookup "response" be of
                  Nothing -> fail "ERROR: could not extract response"
                  Just resp -> return (resp, mi)

-- FIXME: printBar is underbidding the actual progress
printBar :: Int -> (UTCTime, UTCTime) -> (String, Metainfo) -> IO ()
printBar i (iTime, t) (s, mi) = do
  let pieceSize = getPieceLength mi
      total = length s
      done = length $ filter (=='c') s
      inPrg = length $ filter (=='g') s
      percent = done * 100 `div` total
      percentPrg = inPrg * 100 `div` total
  if debugMode
    then do
      printf "%3d%%[%s][%c] <%7s> SPEED %3s\n"
             percent
             (take 100 $ concat [replicate percent '=', replicate percentPrg '>', replicate 100 ' '])
             ("-\\|/" !! (i `mod` 4))
             (showPrettyFilesize $ pieceSize * fromIntegral done)
             (showPrettyTime $ ceiling $ diffUTCTime t iTime)
    else do
      printf blankLine
      hFlush stdout
      printf "%3d%%[%s][%c] <%7s> SPEED %3s"
             percent
             (take 100 $ concat [replicate percent '=', replicate percentPrg '>', replicate 100 ' '])
             ("-\\|/" !! (i `mod` 4))
             (showPrettyFilesize $ pieceSize * fromIntegral done)
             (showPrettyTime $ ceiling $ diffUTCTime t iTime)
      hFlush stdout

blankLine :: String
blankLine = replicate 160 '\b'
