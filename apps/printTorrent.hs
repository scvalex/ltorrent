import Data.Metainfo
import System.Environment
import qualified Data.ByteString.Lazy.Char8 as L

main :: IO ()
main = do
  args <- getArgs
  case args of
    [fi] -> do
         txt <- L.readFile fi
         case parseMetainfo txt of
           Nothing -> putStrLn "couldn't parse file"
           Just mi -> do
              print mi
              print $ getPiecesList $ head mi
    xs   -> do
         putStrLn "wrong number of arguments"
         if length xs < 1
           then putStrLn "try more"
           else putStrLn "try less"
