-- |
-- Module      :  Netowork.Entity
-- Copyright   :  (c) Alexandru Scvortov 2008
-- License     :  LGPL (see LICENSE file)
-- Maintainer  :  scvalex@gmail.com
--

module Network.Entity
    ( Entity(..)
    ) where

-- | A network entity.
--
-- e.g. a Peer, a Tracker; anything with an IP and a port, really
class Entity a where
    -- | returns the IP of the entity
    getIP :: a -> String

    -- | returns the port of the entity
    getPort :: a -> Int
