-- |
-- Module      :  Network.HTTP.Extras
-- Copyright   :  (c) Alexandru Scvortov 2008
-- License     :  LGPL (see LICENSE file)
-- Maintainer  :  scvalex@gmail.com
--

module Network.HTTP.Extras
    ( httpGET
    ) where

import Network.URI (parseURI)
import Network.HTTP (simpleHTTP, Request(..), RequestMethod(..), Response(..))

import Text.Printf (printf)

-- | perform a GET on the query and return the response string
httpGET :: String -> IO String
httpGET uri = do
  case parseURI uri of
    Nothing -> error $ printf "httpGET: uri malformed: ``%s''" uri
    Just u  -> do
                resp <- simpleHTTP (Request u GET [] "")
                case resp of
                  Left err -> error $ printf "httpGET: failed query: ``%s''" uri
                  Right (Response _ _ _ body) -> return body
