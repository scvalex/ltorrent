/**
 * @author Alexandru Scvortov <scvalex@gmail.com>
 * @copyright 2009 Alexandru Scvortov
 * @package webtorrent
 * @license LGPL
 * @url http://github.com/scvalex/ltorrent
 * @require prototype.js scriptaculous.js table.js
 */

var serverAddress = "localhost:8000";

var torrentListUpdater = null;

/* The speed is calculated as the average speed of the last
SPPED_SAMPLES seconds. */
var SPEED_SAMPLES = 15;

function logit(s) {
    if (window["console"] && console.log)
	console.log(s);
}

/* Normalize an object's attibute: Go down the path ps in the object
obj.  If a null is encountered, the default value dflt is returned. */
function normalizeAttribute(obj, ps, dflt) {
    if (typeof(ps) == "string")
	ps = [ps];

    for (var i = 0; i < ps.length; ++i) {
	var p = ps[i];
	if ((obj[p] != null) && (obj[p] != undefined))
	    obj = obj[p];
	else
	    return dflt;
    }
    return obj;
}

function prettyPrintSize(sz) {
    if (sz == 0)
	return "0";

    if (sz >= 1024*1000*1000)
	return ((sz/1024/1000/1000).toFixed(2) + " GB");

    if (sz >= 1024*1000)
	return ((sz/1024/1000).toFixed(2) + " MB");

    if (sz >= 1024)
	return ((sz/1024).toFixed(2) + " KB");

    return (sz + " B");
}

function dateify(n) {
    var nn = n.toFixed(0) + "";
    if (nn.length < 2)
	return "0" + nn;
    return nn;
}

function prettyPrintTime(t) {
    return dateify(t / 3600) + ":" + dateify(t / 60 % 60) + ":" + dateify(t % 60);
}

function splitGroups(n, s, c) {
    if (!c)
	c = ' ';

    var r = "";
    for (var i = 0; i < s.length; i += n) {
	r = r.concat(c, s.slice(i, i+n));
    }

    return r.slice(c.length);
}

function avg(a) {
    var t = 0.0;
    for (var i = 0; i < a.length; ++i)
	t += a[i];
    return t / a.length;
}

function prettyPrintSpeed(s) {
    return (s / 1024).toFixed(2) + " Kb/s";
}

function countOccurrences(a, w) {
    var c = 0;
    for (var i = 0; i < a.length; ++i)
	if (a[i] == w)
	    ++c;
    return c;
}

/* Your vanilla Element.extend but without support for scripts. */
Object.extend(Element.Methods, {
    mUpdate: function(element, content) {
	element = $(element);
	if (content && content.toElement) content = content.toElement();
	if (Object.isElement(content)) return element.mUpdate().insert(content);
	content = Object.toHTML(content);
	element.innerHTML = content;
	return element;
    }
});
Element.addMethods();

function ratioToPercent(d, n) {
    return (d*100/n).toFixed(1);
}

function replicate(w, c) {
    var r = "";
    for (var i = 0; i < c; ++i)
	r += w;
    return r;
}

var Torrent = Class.create({
    initialize: function(t, ih) {
	this.infohash = ih;
	this.metainfo = t.metainfo;
	this.peers = t.peers;
	this.pieceStatus = t.pieceStatus;
	this.swarmPieceStatus = replicate("d", this.pieceStatus.length);
	this.oldDl = this.getDownloaded();
	this.timeElapsed = normalizeAttribute(t, "timeElapsed", 0);
	this.speeds = new Array(SPEED_SAMPLES);
	for (var i = 0; i < this.speeds.length; ++i)
	    this.speeds[i] = 0;
	this.speedsI = 0;
	this.nLeechers = normalizeAttribute(t, "leechers", 0);
	this.nSeeders = normalizeAttribute(t, "seeders", 0);
	this.downloadDirectory = t.downloadDirectory;

	/* psUpdater is an Ajax.PeriodicalUpdater created by
	createTorrent that updates this.pieceStatus */
	this.psUpdater = null;

	/* tsUpdater is an Ajax.PeriodicalUpdater created by
	createTorrent that updates this.trackers */
	this.trackers = [];
	this.tsUpdater = null;

	/* pesUpdater is an Ajax.PeriodicalUpdater created by
	createTorrent that updates this.peers */
	this.peers = [];
	this.newPeers = []; /* peers to display during next ui update */
	this.rmPeers = []; /* peers to remove during next ui update */
	this.pesUpdater = null;
    },

    getName: function() {
	return normalizeAttribute(this, ["metainfo", "info", "name"], "Unknown");
    },

    getSize: function() {
	/*var fs = normalizeAttribute(this, ["metainfo", "info", "files"], []);
	var totalSize = fs.inject(0, function(t, f) {
	    return t + normalizeAttribute(f, "length", 0);
	});
	return totalSize;*/
	return (this.pieceStatus.length * this.getPieceLength());
    },

    getPrettyCreationDate: function() {
	if (!this.metainfo["creation date"])
	    return null;
	var d = new Date(this.metainfo["creation date"] * 1000);
	return d.getFullYear() + "-" + (d.getMonth()+1) + "-" + d.getDate();
    },

    getCreatedByString: function() {
	var client = normalizeAttribute(this, ["metainfo", "created by"], "the Cowardly Client");
	var cd = this.getPrettyCreationDate();
	if (cd)
	    return "On " + cd + " by " + client;
	return "By " + client;
    },

    getPrettyInfohash: function() {
	return splitGroups(5, this.infohash);
    },

    getComment: function() {
	return normalizeAttribute(this, ["metainfo", "comment"], "None");
    },

    getDestination: function() {
	return normalizeAttribute(this, ["downloadDirectory"], "/dev/null");
    },

    getPieceLength: function() {
	return normalizeAttribute(this, ["metainfo", "info", "piece length"], 0);
    },

    getNumPieces: function() {
	return this.pieceStatus.length;
    },

    getDownloaded: function() {
	if (!this.pieceStatus)
	    return 0;
	var npd = countOccurrences(this.pieceStatus, "c");
	return npd * this.getPieceLength();
    },

    getPercentCompleted: function() {
	var d = this.getDownloaded();
	var s = this.getSize();
	return ratioToPercent(d, s);
    },

    getPercentAvailable: function() {
	return ratioToPercent(countOccurrences(this.swarmPieceStatus, "c"), this.pieceStatus.length);
    },

    getTimeElapsed: function() {
	return prettyPrintTime(this.timeElapsed);
    },

    /* Adds a speed to the speed list. */
    considerSpeed: function(s) {
	this.speeds[this.speedsI++] = s;
	if (this.speedsI >= SPEED_SAMPLES)
	    this.speedsI = 0;
    },

    getAvgSpeed: function() {
	var a = avg(this.speeds);
	if (isNaN(a))
	    return 0;
	return a;
    },

    getRemaining: function() {
	return ((this.getSize() - this.getDownloaded()) / this.getAvgSpeed()).toFixed(0);
    }
});

var torrents = new Hash();

function createTorrent(ih) {
    new Ajax.Request("torrent/"+ih, {
	method: "get",
	evalJSON: true,
	onSuccess: function(transport) {
	    var t = transport.responseJSON;
	    if (normalizeAttribute(t, ["response", "metainfo"], null)) {
		torrents.set(ih, new Torrent(t.response, ih));
		displayAddTorrent(ih);
		var tor = torrents.get(ih);
		tor.psUpdater = setupPieceStatusUpdater(ih);
		tor.tsUpdater = setupTrackersUpdater(ih);
		//tor.perUpdater = setupPeersUpdater(ih);
	    }
	},
	onFailure: function() {
	    logit("ERROR: Failed to initialize torrent " + ih);
	}
    });
}

/* Returns null if the resoponse is not JSON, if it has an error field
set or if it doesn't have a response field. Otherwise, returns the
value of the response field. */
function evalResponse(r) {
    if (!r.responseJSON)
	return null;
    r = r.responseJSON;
    if (r.error && (r.error.length > 0))
	return null;
    if (r.response)
	return r.response;
    return null;
}

/* Normalize a JSON passed tracker. */
function makeTracker(r) {
    return {
	url: normalizeAttribute(r, "url", ""),
	peers: normalizeAttribute(r, "peers", 0),
	seeds: normalizeAttribute(r, "seeds", 0),
	status: normalizeAttribute(r, "status", "Unknown")
    };
}

/* Normalize a JSON passed peer. */
function makePeer(r) {
    return {
	ip: normalizeAttribute(r, "ip", "0.0.0.0"),
	pieces: normalizeAttribute(r, "numPieces", 0),
	client: normalizeAttribute(r, "client", "Unknown")
    };
}

function getPeerCompleted(t, p) {
    return ratioToPercent(p.pieces, t.getNumPieces());
}

function setupPeersUpdater(ih) {
    return new Ajax.PeriodicalUpdater(
	"",
	"/torrent/"+ih+"/peers",
	{
	    method: "get",
	    frequency: 10,
	    evalJSON: true,
	    onSuccess: function(transport) {
		var ts = evalResponse(transport);
		if (ts) {
		    var changed = false;
		    var nps = ts.map(makePeer);
		    var ops = torrents.get(ih).peers;
		    var fps = []; /* peers to be added */
		    nps.each(function(p) {
			var op = null;
			/*logit("Still current peers: " + ops);*/
			if (!(op = ops.find(function(cp) cp.ip == p.ip ))) {
			    fps.push(p);
//			    logit("Pushed a peer");
			} else {
//			    logit("Overwrote " + op);
			    op = ops.indexOf(op);
			    ops[op] = p;
			}
		    });
		    torrents.get(ih).newPeers = torrents.get(ih).newPeers.concat(fps);
		    var rmps = []; /* peers to be removed */
		    ops.each(function(p) {
			if (!nps.find(function(cp) cp.ip == p.ip))
			    rmps.push(p);
		    });
		    torrents.get(ih).rmPeers = torrents.get(ih).rmPeers.concat(rmps);
		} else {
		    logit("WARNING: peersUpdater got unusable response");
		}
//	    logit("I'm done");
	    }
	}
    );
}

function setupTrackersUpdater(ih) {
    return new Ajax.PeriodicalUpdater(
	"",
	"/torrent/"+ih+"/trackers",
	{
	    method: "get",
	    frequency: 20,
	    evalJSON: true,
	    onSuccess: function(transport) {
		var ts = evalResponse(transport);
		if (ts)
		    torrents.get(ih).trackers = ts.map(makeTracker);
		else
		    logit("WARNING: trackersUpdater got unusable response");
	    }
	}
    );
}

function setupPieceStatusUpdater(ih) {
    return new Ajax.PeriodicalUpdater(
	"",
	"/torrent/"+ih+"/piecestatus",
	{
	    method: "get",
	    frequency: 2,
	    evalJSON: true,
	    onSuccess: function(transport) {
		var ps = evalResponse(transport);
		// FIXME: if server is down, ps becomes an XMLHttpRequest.  Why?
		var t = torrents.get(ih);
		if (ps) {
		    t.pieceStatus = normalizeAttribute(ps, "pieceStatus", t.pieceStatus);
		    t.swarmPieceStatus = normalizeAttribute(ps, "swarmPieceMap", t.swarmPieceStatus);
		} else {
		    logit("WARNING: pieceStatusUpdater got unusable response");
		}
	    }
	}
    );
}

/* FIXME: updateTorrentListRow should NOT alter Torrent */
function updateTorrentListRow(tr) {
    if (!tr)
	return;
    var ih = tr.id;
    var t = torrents.get(ih);
    if (!t)
	return;
    var dl = t.getDownloaded();
    tr.select(".downloaded")[0].mUpdate(prettyPrintSize(dl));
    tr.select(".complete")[0].mUpdate(t.getPercentCompleted() + "%");
    t.considerSpeed(dl - t.oldDl);
    t.oldDl = dl;
    tr.select(".speed")[0].mUpdate(prettyPrintSpeed(t.getAvgSpeed()));
    ++t.timeElapsed;
    tr.select(".time")[0].mUpdate(t.getTimeElapsed());
}

function setupTorrentListUpdater() {
    if (torrentListUpdater) {
	torrentListUpdater.stop();
	torrentListUpdater = null;
    }

    /* Periodically check for new torrents. */
    torrentListUpdater = new Ajax.PeriodicalUpdater(
	"",
	"/torrents",
	{
	    method: "get",
	    frequency: 4,
	    evalJSON: true,
	    onSuccess: function(transport) {
		var ts = evalResponse(transport);
		if (!ts)
		    logit("WARNING: torrentListUpdater got unusable response");
		ts.each(function(t) {
		    if (!torrents.get(t.infohash))
			createTorrent(t.infohash);
		});
	    }
	}
    );

    /* This PE only handles the display part; each Torrent has its own
    Ajax.PU for updating. */
    new PeriodicalExecuter(function() {
	$$("#torrents tr").each(updateTorrentListRow);
	var at = $$("#torrents tr.active")[0];
	if (at)
	    updateInfoToTorrent(at.id);
    }, 1);
}

function connect() {
    /* FIXME: Even if the server falls, connect will still report
       the connection as established.  As far as I can tell, the AJAX
       response is somehow or appears to be buffered. */
    logit("Connecting to " + serverAddress + "...");
    
    new Ajax.Request("/stat", {
	method: "get",
	onSuccess: function(t) {
	    logit("...connected");
	    setupTorrentListUpdater();
	},
	onFailure: function(t) {
	    logit("...NOT connected");
	}
    });
}

function concat(ss, sp) {
    if (!sp)
	sp = "";
    var t = "";
    for (var i = 0; i < ss.length; ++i)
	t = t.concat(sp, ss[i]);
    return t.slice(sp.length);
}

function dropAllButAlphaNums(s) {
    var r = "";
    for (var i = 0; i < s.length; ++i)
	if ((('A' <= s[i]) && (s[i] <= 'z')) || (('0' <= s[i]) && (s[i] <= '9')))
	    r += s[i];
    return r;
}

function redrawBar(b, s) {
    if (!s) {
	logit("Crap.  No torrent.");
	return;
    }

    b = $(b);
    var ctx = b.getContext("2d");
    ctx.fillStyle = "rgb(220, 220, 255)";
    ctx.fillRect(0, 0, b.width, b.height);
    var tw = (b.width / s.length).toFixed(0);
    for (var i = 0; i < s.length; ++i) {
	ctx.fillStyle = "rgb(255, 0, 0)";
	if (s[i] == 'g')
	    ctx.fillStyle = "rgb(0, 0, 125)";
	else if (s[i] == "c")
	    ctx.fillStyle = "rgb(0, 0, 255)";
//	logit("i*tw=" + (i*tw) + " tw=" + tw);
	ctx.fillRect(i * tw, 0, tw, b.height);
    }
}

function updateInfoToTorrent(ih) {
    var t = torrents.get(ih);
    if (!t)
	loggit("No torrent with infohash " + ih);

    /* Update General tab only if selected */
    if (tabs.activeContainer.id == "tabGeneral") {
	$("timeElapsedLabel").mUpdate(t.getTimeElapsed());
	$("downloadedLabel").mUpdate(prettyPrintSize(t.getDownloaded()));
	var s = t.getAvgSpeed();
	$("downloadSpeedLabel").mUpdate(prettyPrintSpeed(s));
	if (s > 0)
	    $("statusLabel").mUpdate("Downloading");
	else
	    $("statusLabel").mUpdate("Stalled");
	$("uploadedLabel").mUpdate(prettyPrintSize(0));
	$("uploadSpeedLabel").mUpdate(prettyPrintSpeed(0));
	var r = t.getRemaining();
	if ((r == Infinity) || (r == -Infinity))
	    r = "oo";
	else
	    r = prettyPrintTime(r);
	$("remainingLabel").mUpdate(r);
	$("leechersLabel").mUpdate(t.nLeechers);
	$("seedsLabel").mUpdate(t.nSeeders);

	$("destinationLabel").mUpdate(t.getDestination());
	$("totalSizeLabel").mUpdate(prettyPrintSize(t.getSize()) 
				    + " (" + prettyPrintSize(t.getDownloaded()) + " done)");
	$("infohashLabel").mUpdate(t.getPrettyInfohash());
	$("createdLabel").mUpdate(t.getCreatedByString());
	$("commentLabel").mUpdate(t.getComment());

	redrawBar("downloadedBar", t.pieceStatus);
	redrawBar("availabilityBar", t.swarmPieceStatus);
	$("downloadedPercent").mUpdate((t.getPercentCompleted()/100).toFixed(2));
	$("availabilityPercent").mUpdate((t.getPercentAvailable()/100).toFixed(2));
    }

    /* Set Files tab only when torrent selection changes */
    if (ih != $("info").infohash) {
	fileList.clear();
	var fs = normalizeAttribute(t, ["metainfo", "info", "files"], []);
	$A(fs).each(function(f) {
            //logit(concat(f.path, "/"));
	    fileList.addRow([
		concat(f.path, "/"),
		prettyPrintSize(f.length),
		/*"0.00%"*/
	    ]);
	});
    }

    /* Set Trackers tab only when torrent selection changes */
    /* FIXME: tracker table update is inefficient */
    if ((ih != $("info").infohash) || (trackerList.numRows != t.trackers.length)) {
	trackerList.clear();
	t.trackers.each(function(t) {
	    trackerList.addRow([
		t.url,
		new Element("td", { "class": "status" }).mUpdate(t.status),
		new Element("td", { "class": "seeds" }).mUpdate(t.seeds),
		new Element("td", { "class": "peers"}).mUpdate(t.peers)
	    ], { id: dropAllButAlphaNums(t.url) });
	});
    }

    /* Init Peers tab only when torrent selection changes or when
    peers have to be added */
    /* FIXME: peer table update is inefficient */
    /*if ((ih != $("info").infohash) || (t.newPeers.length > 0)) {
	if (ih != $("info").infohash) {
	    peerList.clear();*/
	    /*for (p in t.peers)
		if (t.newPeers.find(function(np) np.ip == p.ip))
		    logit("Peer " + p + " appears both in peers and newPeers");*/
	    //t.newPeers = t.peers.concat(t.newPeers); /* FIXME: duplicate rows sometimes appear */
	    //logit(t.newPeers);
	    /*t.peers = [];
	}
	t.newPeers.each(function(p) {
		peerList.addRow([
		    p.ip,
		    new Element("td", { "class": "client" }).mUpdate(p.client),
		    new Element("td", { "class": "complete" }).mUpdate(getPeerCompleted(t, p) + "%"),
		    new Element("td", { "class": "downloaded" }).mUpdate("0"),
		    new Element("td", { "class": "downspeed" }).mUpdate("0 Kb/s"),
		    new Element("td", { "class": "uploaded" }).mUpdate("0"),
		    new Element("td", { "class": "upspeed" }).mUpdate("0 Kb/s")
		], { id: dropAllButAlphaNums(p.ip) });
	});*/
	//t.peers = t.peers.concat(t.newPeers); /* FIXME: remove stale peers somewhere */
	/*t.newPeers = [];
    }*/

    /* Update Trackers tab only if selected */
    if (tabs.activeContainer.id == "tabTrackers") {
	var ac = tabs.activeContainer;
	t.trackers.each(function(t) {
	    var tr = trackerList.body.select("#"+dropAllButAlphaNums(t.url))[0];
	    if (!tr)
		return;
	    tr.select(".status")[0].mUpdate(t.status);
	    tr.select(".seeds")[0].mUpdate(t.seeds);
	    tr.select(".peers")[0].mUpdate(t.peers);
	});
    }

    /* Update Peers tab only if selected */
    /*if (tabs.activeContainer.id == "tabPeers") {
	var ac = tabs.activeContainer;
	t.rmPeers.each(function(p) {
	    var tr = peerList.body.select("#"+dropAllButAlphaNums(p.ip))[0];
	    if (!tr)
		return;
	    peerList.removeRow(tr);
	    //logit("removed a stale row");
	});
	t.rmPeers = [];
	t.peers.each(function(p) {
	    var tr = peerList.body.select("#"+dropAllButAlphaNums(p.ip))[0];
	    if (!tr)
		return;
	    tr.select(".client")[0].mUpdate(p.client);
	    tr.select(".complete")[0].mUpdate(getPeerCompleted(t, p) + "%");
	    tr.select(".downloaded")[0].mUpdate("0");
	    tr.select(".downspeed")[0].mUpdate("0 Kb/s");
	    tr.select(".uploaded")[0].mUpdate("0");
	    tr.select(".upspeed")[0].mUpdate("0 Kb/s");
	    //logit("updated a row");
	});
    }*/
    
    $("info").infohash = ih;
}

/* Unselects previously selected torrent, marks selected torrent as
active and updates the info part to show the current torrent. */
function selectTorrent(ih) {
    var oldIh = $$("#torrentList tbody tr.active");
    if (oldIh[0])
	oldIh = oldIh[0].id;
    if (ih != oldIh) {
	$$("#torrents tr.active").each(function(r) {
	    r.removeClassName("active");
	});
	$(ih).addClassName("active");

	updateInfoToTorrent(ih);
    }
}

function bsearch(l, u, needToEnlarge) {
    while (l < u) {
	var m = l + ((u-l)/2).floor();
	if (needToEnlarge(m))
	    l = m+1;
	else
	    u = m;
    }
    return l;
}

function fitText(pxs, text, cls) {
    if (!cls)
	cls = "";
    var rpxs = $("dummy").insert(new Element("span", {"class": cls}).mUpdate(text)).getWidth();
    $("dummy").mUpdate();
    if (rpxs <= pxs)
	return text.length;
    return bsearch(1, text.length, function(m) {
	var l = $("dummy").insert(new Element("span", {"class": cls}).mUpdate(text.truncate(m))).getWidth();
	$("dummy").mUpdate();
	if (l < pxs)
	    return true;
	return false;
    });
}

function displayAddTorrent(ih) {
    //logit(torrents.get(ih).metainfo);
    var t = torrents.get(ih);
    var hmc = fitText($("nameCol").getWidth(), t.getName(), "name");
    torrentList.addRow([
	new Element("td", { "class": "name" }).mUpdate(t.getName().truncate(hmc)),
	new Element("td", { "class": "downloaded" }),
	new Element("td", { "class": "size" }).mUpdate(prettyPrintSize(t.getSize())),
	new Element("td", { "class": "complete" }),
	new Element("td", { "class": "speed" }),
	new Element("td", { "class": "time" })],
	{"class": "torrentRow", id: ih}
    ).observe("click", function() {
	selectTorrent(ih);
    });
    updateTorrentListRow($(ih));
    if ($$("#torrents tr").length == 1)
	selectTorrent(ih);
}

function isDigit(c) {
    return (('0' <= c) && (c <= '9'));
}

function takeWhile(f, s) {
    var r = "";
    for (var i = 0; i < s.length; ++i) {
	if (f(s[i]))
	    r += s[i];
        else
	    break;
    }
    return r;
}

function hideDevPanel() {
    new Effect.DropOut("devPanel");
    $("showDevPanelButton").mUpdate("show panel");
}

function insertCanvas(e, c) {
    e = $(e);
    e.insert(new Element("canvas", { 
	id: c,
	width: e.getWidth()-8 + "px",
	height: e.getHeight()+2 + "px"
    }));
}

document.observe("dom:loaded", function() {
    $("showDevPanelButton").observe("click", function() {
	if ($("devPanel").visible()) {
	    hideDevPanel();
	} else {
	    new Effect.Appear("devPanel", { duration: .3 });
	    $("showDevPanelButton").mUpdate("hide panel");
	}
    });

    $("hideDevPanelButton").observe("click", hideDevPanel);

    $("clearTorrentsButton").observe("click", function() {
	torrents = new Hash();
	torrentList.clear();
    });

    $("reconnectButton").observe("click", connect);

    torrentList = new Control.Table("torrentList");

    tabs = new Control.Tabs("tabs");

    fileList = new Control.Table("fileList");
    trackerList = new Control.Table("trackerList");
    //peerList = new Control.Table("peerList");
    //pieceList = new Control.Table("pieceList");

    insertCanvas("downloadedBarC", "downloadedBar");
    insertCanvas("availabilityBarC", "availabilityBar");

    connect();
});
