/**
 * @author Alexandru Scvortov <scvalex@gmail.com>
 * @copyright 2009 Alexandru Scvortov
 * @package webtorrent
 * @license LGPL
 * @url http://github.com/scvalex/ltorrent
 * @require prototype.js
 */

if (typeof(Prototype) == "undefined")
    throw "Control.Table needs Prototype to be loaded.";

Control.Table = Class.create({
    initialize: function(table_container, options) {
	if (!$(table_container))
	    throw "Control.Table could not find element " + table_container;

	this.options = {
	    acceptReorders: true
	};
	Object.extend(this.options, options || {});

	this.body = null;
	if ($(table_container).select("tbody").length)
	    this.body = $(table_container).select("tbody")[0];
	else
	    this.body = $(table_container);

	this.numRows = 0;

	if (this.options.acceptReorders) {
	    var th = $(table_container).select("thead");
	    if (th.length > 0) {
		th = th[0];
		th.select("td").each((function(e) {
		    e.observe("click", (function(t) {
			t = t.element();
			var colNum = th.select("td").indexOf(t);
			var tb = $(table_container).select("tbody");
			if (tb.length > 0) {
			    tb = tb[0];
			    var trs = tb.select("tr");

			    if (!t.ordering)
				t.ordering = 1;

			    trs.sort(function(a, b) {
				a = a.select("td")[colNum];
				if (a)
				    a = a.innerHTML;
				b = b.select("td")[colNum];
				if (b)
				    b = b.innerHTML;
				if (a < b)
				    return -1 * t.ordering;
				if (a > b)
				    return 1 * t.ordering;
				return 0;
			    });
			    t.ordering = -t.ordering;

			    tb.update();
			    trs.each(this.addRow.bind(this));

			    t.up("tr").select("td.sortedAscending").each(function(td) {
				td.removeClassName("sortedAscending");
			    });
			    t.up("tr").select("td.sortedDescending").each(function(td) {
				td.removeClassName("sortedDescending");
			    });
			    if (t.ordering == -1)
				t.addClassName("sortedAscending");
			    else
				t.addClassName("sortedDescending");
			}
		    }).bind(this));
		}).bind(this));
	    }
	}
    },

    addRow: function(es, options) {
	/* The following condition is crap. */
	if (!es.length) {
	    this.body.insert(es);
	    return;
	}
	var tr = new Element("tr", options);
	es.map(function(e) {
	    /* The following condition is crap. */
	    if (typeof(e) == "object")
		return e;
	    return new Element("td").update(e);
	}).each(function(td) {
	    tr.insert(td);
	});
	this.body.insert(tr);
	this.numRows++;
	return tr;
    },

    removeRow: function(e) {
	if (!e)
	    return;

	var tr = e.remove();
	this.numRows--;
	return tr;
    },

    clear: function() {
	this.body.update();
	this.numRows = 0;
    }
});
