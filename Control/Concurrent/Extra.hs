-- |
-- Module      :  Control.Concurrent.Extra
-- Copyright   :  (c) Alexandru Scvortov 2008
-- License     :  LGPL (see LICENSE file)
-- Maintainer  :  scvalex@gmail.com
--

module Control.Concurrent.Extra
    ( noisySpawnIO
    , forkIOThrowErrorsHere
    , forkIOWithTimeout
    , timeoutMaybe
    , modifyTVar
    , inDo
    ) where

import Control.Concurrent
import Control.Concurrent.STM ( STM, TVar, readTVar, writeTVar )
import Control.Exception.Extra
import Control.Monad
import Control.Monad.Trans
import Text.Printf ( printf )

-- LESSON: Design from the beginning
--
-- I learned something interesteing yesterday.  Here is what happened:
-- I have been working on this programme for some time now.  When I
-- started, I was more interested in getting things done than in doing
-- them well.  This, coupled with lack of experience, led me to
-- believe that error handling was unimportant -- a trivial detail I
-- would sort out later.  Needless to say, I was wrong.
--
-- After settling on a way to report the download's progress to the
-- user, I started removing the various prints littered around the
-- code; this was easy to do.  Harder was tracking down all the
-- uncaught exceptions that would bubble up to the surface when
-- threads died; again, I got most of them quickly.  But, there was
-- one I couldn't seem to catch.  I checked the code again and again.
-- I narrowed down the search first to the function, then to line.  I
-- tried throwing other exceptions around the code and they were all
-- caught.  I rewrote the line, but still, the troublesome exception
-- got out.
--
-- What could be happening?  I found out after two days of searching.
-- The timeout function (used in the guilty statement) would run the
-- action passed to it in another thread; the action would fault and
-- the thread would die.  The calling thread would be oblivious to
-- this and would simply report that the action timed out.
--
-- MORAL: Consider all things from the beginning, otherwise they will
-- crop up unexpectadley and waste you most precious resource -- time.
 

-- | Fork and do action and do something afterwards.  Also, make a lot
-- of noise, should an exception be raised.
--
-- What on earth could be the use of this you ask?  Well, take for
-- example the story of theBridge.  This was a thread that ran the
-- function bearing the same name.  For some unknown reason, it kept
-- dying (I knew this because I had added a finalizer to end of
-- spawnIO).  For almost a month, I tried to figure out what was
-- happening.  I tried everything and made countless observations such
-- as "it takes longer for it to die if there's no disk io done" or
-- "it doesn't die if threads so and so aren't active".  Of course,
-- these were completely useless.  One day I thought (yes, I know, it
-- sometimes happens even to me) about it and realized I did not know
-- what exception was occuring.  So I quickly wrote noisySpawnIO, a
-- talkative drop-in replacement for spawnIO.  It immediately showed
-- that GHC's runtime had deduced that the thread blocked
-- indefinitely.
--
-- MORAL: a few moments of clear thought can save you a month of
-- agony.
noisySpawnIO :: IO () -- ^ action to do
             -> IO () -- ^ what to do after action
             -> IO ()
noisySpawnIO act finalizer = do
  forkIO $ do
    act `catchAll` (\e -> printf "AN EXCEPTION OCCURED: %s\n" (show e))
        `finally` finalizer
  return ()

-- | Fork a thread but catch exceptions and throw them to the calling
-- thread.
forkIOThrowErrorsHere :: IO () -> IO ThreadId
forkIOThrowErrorsHere act = do
  mytid <- myThreadId
  forkIO $ act `catchAll` \e -> throwTo mytid e

-- | A threadDelay that works with Integers rather than Ints.  This
-- | means you can now have delays longer than ~35mins.
lessRetardedThreadDelay :: Integer -> IO ()
lessRetardedThreadDelay i = if i < 2000000000
                            then threadDelay (fromIntegral i)
                            else threadDelay 2000000000 >> lessRetardedThreadDelay (i - 2000000000)

-- | Fork and do action but abort if so-and-so seconds pass.
forkIOWithTimeout :: Integer   -- ^ timeout in MILIseconds
                  -> IO () -- ^ action to do
                  -> IO () -- ^ timeout action
                  -> IO ThreadId
forkIOWithTimeout n act iftimeout = forkIOThrowErrorsHere $ do
    blk <- newEmptyMVar
    aid <- forkIOThrowErrorsHere (do {act; putMVar blk (return ())})
    bid <- forkIO (do {lessRetardedThreadDelay (n*1000); putMVar blk iftimeout})
    nxt <- takeMVar blk
    mapM_ killThread [aid, bid]
    nxt

-- | Run an action; if too much time passes, return Nothing.
timeoutMaybe :: Integer  -- ^ timeout in MILIseconds
             -> IO a     -- ^ action
             -> IO (Maybe a)
timeoutMaybe n act = do
  r <- newEmptyMVar
  aid <- forkIOThrowErrorsHere (do {x <- act; putMVar r (Just x)})
  bid <- forkIO (do {lessRetardedThreadDelay (n*1000); putMVar r Nothing})
  val <- takeMVar r
  mapM_ killThread [aid, bid]
  return val
  
-- | Apply the function over the TVar and replaces its content.
modifyTVar :: TVar a -> (a -> a) -> STM ()
modifyTVar tv f = readTVar tv >>= writeTVar tv . f

-- | Execute an action in so many MILIseconds.
inDo :: MonadIO m => Integer -> m a -> m a
inDo ms a = liftIO (lessRetardedThreadDelay (ms * 1000)) >> a
