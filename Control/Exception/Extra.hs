-- |
-- Module      :  Control.Exception.Extra
-- Copyright   :  (c) Alexandru Scvortov 2009
-- License     :  LGPL (see LICENSE file)
-- Maintainer  :  scvalex@gmail.com
--
-- Use this instead of Control.Exception*.
--

module Control.Exception.Extra
    ( catch, catchAll
    , finally
    ) where

import Prelude hiding ( catch )

import Control.Exception

-- | Catch all exceptions.
catchAll :: IO a -> (SomeException -> IO a) -> IO a
catchAll = catch
